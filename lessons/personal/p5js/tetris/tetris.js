class Tetris {
    constructor(cellWidth, cellHeight) {
        this.cols = 12;
        this.rows = 30;
        this.cellWidth = cellWidth;
        this.cellHeight = cellHeight;
        this.state = [];
        this.shapes = [];
    }

    generateState() {
        for (let row = 0; row <= this.rows; row++) {
            this.state[row] = [];
            for (let col = 0; col <= this.cols; col++)
                this.state[row][col] = null;
        }
    }

    drawGrid() {
        for (let row = 0; row <= this.rows; row++) {
            line(0, row * this.cellHeight, this.cols * this.cellWidth, row * this.cellHeight);
        }
        for (let col = 0; col <= this.cols; col++) {
            line(col * this.cellWidth, 0, col * this.cellWidth, this.rows * this.cellHeight);
        }
    }

    updateState() {
        for (let shape of this.shapes) {
            for (let point of shape) {
                rect(point[0] * this.cellWidth, point[1] * this.cellHeight, this.cellWidth, this.cellHeight);
            }
        }
    }
}