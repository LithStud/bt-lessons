class Shape {
    constructor(shape, cellWidth, cellHeight) {
        this.cells = shape;
        this.cellWidth = cellWidth;
        this.cellHeight = cellHeight;
    }

    draw() {
        for (let block of this.cells) {
            rect(block.col * this.cellWidth, block.row * this.cellHeight, this.cellWidth, this.cellHeight);
        }
    }

    move(mode) {
        switch (mode) {
            case 'left':
                for (let cell of this.cells) {
                    cell.col--;
                    if (cell.col < 0) {
                        cell.col = 0;
                        break;
                    }
                }
                break;

            case 'right':
                for (let cell of this.cells) {
                    cell.col++;
                    if (cell.col > 11) {
                        cell.col = 11;
                        break;
                    }
                }
                console.log(this.cells);
                break;

            default:
                break;
        }
    }
}