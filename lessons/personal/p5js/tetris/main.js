let width = 500;
let height = 500;

let cell = {
    w: 15,
    h: 15,
}

let tetris = new Tetris(cell.w, cell.h);
let shape = new Shape('', cell.w, cell.h);

function setup() {
    createCanvas(width, height);
    background(0);
    tetris.generateState();
    translate(10, 10);
    tetris.updateState();
    //tetris.drawState();
}

function draw() {
    background(0);
    stroke(255, 50);
    strokeWeight(1);
    tetris.drawGrid();
    //shape.draw();

    stroke(0);
    for (let x = 0; x < 3; x++) {
        beginShape();
        vertex(x * cell.w, 0);
        vertex((x + 1) * cell.w, 0);
        vertex((x + 1) * cell.w, cell.h);
        vertex(x * cell.w, cell.h);
        endShape();
    }
    beginShape();
    vertex(1 * cell.w, cell.h * 1);
    vertex((1 + 1) * cell.w, cell.h * 1);
    vertex((1 + 1) * cell.w, cell.h * (1 + 1));
    vertex(1 * cell.w, cell.h * (1 + 1));
    endShape();

}

function keyPressed() {
    if (keyCode === LEFT_ARROW) {
        shape.move('left');
    }

    if (keyCode === RIGHT_ARROW) {
        shape.move('right');
    }

}