let width = 500;
let height = 500;
let cell = {
    w: 20,
    h: 20
}
let stuff = [{ x: 1, y: 1 }];
let cols = Math.floor(width / cell.w);
let rows = Math.floor(height / cell.h);

function setup() {
    createCanvas(width, height);
    background(0);
    drawGrid();
}

function draw() {
    for(coord of stuff){
        rect(coord.x * cell.w, coord.y * cell.h, cell.w, cell.h);
    }
}

function drawGrid() {
    stroke(255, 50);
    strokeWeight(1);
    for (let row = 0; row < rows; row++) {
        line(0, row * cell.h, width, row * cell.h);
    }
    for (let col = 0; col < cols; col++) {
        line(col * cell.w, 0, col * cell.w, height);
    }
}