class Board {
    constructor(state = null) {
        this.rows = 3;
        this.cols = 3;
        if (state != null)
            this.state = state;
        else
            this.state = this.generateBoard();
    }

    generateBoard() {
        let state = [];
        for (let row = 0; row < this.rows; row++) {
            state[row] = [];
            for (let col = 0; col < this.cols; col++) {
                state[row][col] = null; // empty cell
            }
        }
        return state;
    }

    markCell(row, col, mark) {
        if (this.state[row][col] === null)
            this.state[row][col] = mark;
        else return false;
        return this.state;
    }

    getEmptyCells() {
        let emptyCells = [];
        for (let row = 0; row < this.rows; row++) {
            for (let col = 0; col < this.cols; col++) {
                if (this.state[row][col] === null) { // empty cell
                    emptyCells.push({ row: row, col: col });
                }
            }
        }
        return emptyCells;
    }

    getStateClone() {
        let clone = [];
        for (let row = 0; row < this.rows; row++) {
            let cols = [];
            for (let col = 0; col < this.cols; col++) {
                cols.push(this.state[row][col]);
            }
            clone.push(cols);
        }
        return clone;
    }

    isEmpty() {
        for (let row = 0; row < this.rows; row++) {
            for (let col = 0; col < this.cols; col++) {
                if (this.state[row][col] !== null) { // empty cell
                    return false;
                }
            }
        }
        return true;
    }

    isFull() {
        for (let row = 0; row < this.rows; row++) {
            for (let col = 0; col < this.cols; col++) {
                if (this.state[row][col] === null) { // empty cell
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Checks for win conditions.
     */
    isTerminal() {
        // check if board is empty
        if (this.isEmpty())
            return false;

        // functions for winner checking
        function winnerPlayerOne(value) {
            return value === 'x';
        };
        function winnerPlayerTwo(value) {
            return value === 'o';
        }

        // horizontal lines
        for (let x = 0; x < this.rows; x++) {
            if (this.state[x].every(winnerPlayerOne))
                return { winner: 'x', type: 'row', coords: x };
            if (this.state[x].every(winnerPlayerTwo))
                return { winner: 'o', type: 'row', coords: x };
        }

        //check vertical lines
        for (let y = 0; y < this.cols; y++) {
            let tempCol = [];
            for (let x = 0; x < this.rows; x++) {
                tempCol.push(this.state[x][y]);
            }
            if (tempCol.every(winnerPlayerOne)) return { winner: 'x', type: 'col', coords: y };
            if (tempCol.every(winnerPlayerTwo)) return { winner: 'o', type: 'col', coords: y };
        }
        //check diagonals
        let diag1 = [], diag2 = [];
        for (let x = 0; x < this.state.length; x++) {
            diag1.push(this.state[x][x]);
            diag2.push(this.state[x][this.state.length - x - 1]);
        }
        if (diag1.every(winnerPlayerOne)) return { winner: 'x', type: 'diag', coords: 'lr' };
        if (diag1.every(winnerPlayerTwo)) return { winner: 'o', type: 'diag', coords: 'lr' };

        if (diag2.every(winnerPlayerOne)) return { winner: 'x', type: 'diag', coords: 'rl' };
        if (diag2.every(winnerPlayerTwo)) return { winner: 'o', type: 'diag', coords: 'rl' };

        // draw
        if (this.isFull())
            return { winner: 'draw' };

        // No winner yet
        return false;
    }

    /**
     * Creates neccesary HTML to display board
     * @param {HTMLElement} rootEl root element where to create board
     */
    drawBoard(rootEl) {
        let root = document.documentElement;
        let totalWidth = window.innerWidth;
        let totalHeight = window.innerHeight;
        let cells = '';
        let boardSize = 0;
        // decide how to fit board on screen
        if (totalHeight > totalWidth) {
            boardSize = totalWidth - totalWidth * 0.1;
        } else {
            boardSize = totalHeight - totalHeight * 0.1;
        }
        root.style.setProperty('--size', boardSize + 'px');
        root.style.setProperty('--gap', 5 + 'px');
        let size = Math.floor(boardSize / this.cols) - 5;
        root.style.setProperty('--cell-size', size + 'px');
        for (let row = 0; row < this.rows; row++)
            for (let col = 0; col < this.cols; col++)
                cells += `<div data-row="${row}" data-col="${col}" class="cell empty"></div>`;
        rootEl.innerHTML = cells;
    }

    updateHtml(rootEl) {
        let root = document.documentElement;
        let totalWidth = window.innerWidth;
        let totalHeight = window.innerHeight;
        let boardSize = 0;
        // decide how to fit board on screen
        if (totalHeight > totalWidth) {
            boardSize = totalWidth - totalWidth * 0.1;
        } else {
            boardSize = totalHeight - totalHeight * 0.1;
        }
        root.style.setProperty('--size', boardSize + 'px');
        root.style.setProperty('--gap', 5 + 'px');
        let size = Math.floor(boardSize / this.cols) - 5;
        root.style.setProperty('--cell-size', size + 'px');
    }
}