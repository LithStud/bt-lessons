
let board = new Board();
let boardEl = document.getElementById('playground');
board.drawBoard(boardEl);
/* board.markCell(0, 0, 'x');
board.markCell(2, 0, 'x');
board.markCell(2, 1, 'x');

board.markCell(0, 2, 'o');
board.markCell(1, 1, 'o');
let move = board.markCell(2, 2, 'o');
checkMove(move); */
/* checkMove(board.markCell(2, 2, 'o'));
checkMove(board.markCell(1, 1, 'x'));

checkMove(board.markCell(2, 0, 'o')); */
//checkMove(board.markCell(2, 1, 'x'));

//checkMove(board.markCell(0, 1, 'o'));
//board.markCell(0, 1, 'x');
//console.log(minimax(board, 9, true));
/* checkMove(board.markCell(0, 0, 'x'));
checkMove(board.markCell(2, 1, 'o'));
checkMove(board.markCell(0, 1, 'x'));
checkMove(board.markCell(2, 3, 'o'));
checkMove(board.markCell(2, 4, 'x')); */
/* console.log(alphabeta(board, 3, Number.NEGATIVE_INFINITY, Number.POSITIVE_INFINITY, true)); */
//console.log(board.getEmptyCells());
//console.log(board.isTerminal());

function checkMove(move) {
    move ? console.table(move) : console.log('Invalid move');
}

/**
 * 
 * @param {Board} node Board
 * @param {Number} depth max depth to go
 * @param {Boolean} maximizingPlayer is maximizing player
 */
function minimax(node, depth, maximizingPlayer) {
    let terminal = node.isTerminal();
    if (depth === 0 || terminal) {
        if (terminal.winner === 'x') return { val: 100, move: null };
        if (terminal.winner === 'o') return { val: -100, move: null };
        return { val: 0, move: null };
    }

    let emptyCells = node.getEmptyCells();
    if (maximizingPlayer) {
        let value = { val: Number.NEGATIVE_INFINITY, move: null };
        for (let move of emptyCells) {
            let child = new Board(node.getStateClone());
            child.markCell(move.row, move.col, 'x');
            let newValue = minimax(child, depth - 1, false);
            if (value.val < newValue.val) {
                value = newValue;
                value.move = move;
            }
            //value = Math.max(value, minimax(child, depth - 1, false));
        }
        //console.log(`MAX | Depth: ${depth} | Value: ${value.val} | [${value.move.row}][${value.move.col}]`);
        return value;
    } else {
        let value = { val: Number.POSITIVE_INFINITY, move: null };
        for (let move of emptyCells) {
            let child = new Board(node.getStateClone());
            child.markCell(move.row, move.col, 'o');
            let newValue = minimax(child, depth - 1, true);
            if (value.val > newValue.val) {
                value = newValue;
                value.move = move;
            }
            //value = Math.min(value, minimax(child, depth - 1, true));
        }
        //console.log(`MIN | Depth: ${depth} | Value: ${value.val} | [${value.move.row}][${value.move.col}]`);
        return value;
    }
}

/**
 * Minimax with Alpha Beta pruning.
 * @param {Board} node Board
 * @param {Number} depth max depth to go
 * @param {Number} alpha alpha value
 * @param {Number} beta beta value
 * @param {Boolean} maximizingPlayer is maximizing player
 */
function alphabeta(node, depth, alpha, beta, maximizingPlayer) {
    let terminal = node.isTerminal();
    if (depth === 0 || terminal) {
        if (terminal.winner === 'x') return { val: 100, move: null };
        if (terminal.winner !== 'o') return { val: -100, move: null };
        return { val: 0, move: null };
    }

    let emptyCells = node.getEmptyCells();
    if (maximizingPlayer) {
        let value = { val: Number.NEGATIVE_INFINITY, move: null };
        for (let move of emptyCells) {
            let child = new Board(node.getStateClone());
            child.markCell(move.row, move.col, 'x');
            let newValue = alphabeta(child, depth - 1, alpha, beta, false);
            if (value.val < newValue.val) {
                value = newValue;
                value.move = move;
            }
            // check alpha beta values and if needed cut off branch
            if (alpha < value.val)
                alpha = value.val;
            if (alpha >= beta)
                break;
        }
        console.log(`MAX | Depth: ${depth} | Value: ${value.val} | [${value.move.row}][${value.move.col}]`);
        return value;
    } else {
        let value = { val: Number.POSITIVE_INFINITY, move: null };
        for (let move of emptyCells) {
            let child = new Board(node.getStateClone());
            child.markCell(move.row, move.col, 'o');
            let newValue = minimax(child, depth - 1, true);
            // check alpha beta values and if needed cut off branch
            if (value.val > newValue.val) {
                value = newValue;
                value.move = move;
            }
            if (beta > value.val)
                beta = value.val;
            if (alpha >= beta)
                break;
        }
        console.log(`MIN | Depth: ${depth} | Value: ${value.val} | [${value.move.row}][${value.move.col}]`);
        return value;
    }
}

let players = {
    x: 'x',
    o: 'o',
    current: 'o',

    mark() {
        let mark = this.current;
        this.current = this.current === this.x ? this.o : this.x;
        return mark;
    }
}

document.addEventListener('click', e => {
    if (board.isTerminal()) return;
    if (e.target.classList.contains('cell') && e.target.classList.contains('empty')) {
        console.log(e.target.dataset.row);
        let mark = players.mark();
        let row = Number(e.target.dataset.row);
        let col = Number(e.target.dataset.col);
        board.markCell(row, col, mark);
        e.target.classList.replace('empty', mark);
        if (!board.isFull()) {
            let aiMove = alphabeta(board, 7, Number.NEGATIVE_INFINITY, Number.POSITIVE_INFINITY, true);
            mark = players.mark();
            board.markCell(aiMove.move.row, aiMove.move.col, mark);
            let aiCell = boardEl.querySelector(`[data-row="${aiMove.move.row}"][data-col="${aiMove.move.col}"]`);
            aiCell.classList.replace('empty', mark);
        }
    }
});

window.addEventListener("resize", resizeThrottler, false);

var resizeTimeout;
function resizeThrottler() {
    // ignore resize events as long as an actualResizeHandler execution is in the queue
    if (!resizeTimeout) {
        resizeTimeout = setTimeout(function () {
            resizeTimeout = null;
            windowResized();

            // The actualResizeHandler will execute at a rate of 15fps
        }, 66);
    }
}

// handle event
function windowResized() {
    board.updateHtml(boardEl);
}