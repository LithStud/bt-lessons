import Board from './Board.js';

class Player {
    /**
     * Player Class - used for AI
     * @param {String} playerMark Player used marking
     * @param {Number} maxDepth How deep to predict (default -1)
     */
    constructor(playerMark, maxDepth = -1) {
        this.mark = playerMark;
        this.maxDepth = maxDepth;
        this.nodes = [];
    }

    /**
     * Minimax algorythm with Alpha-Beta pruning
     * @param {Board} board Board Object
     * @param {Number} depth Starting depth
     * @param {Boolean} maximizing Which player move
     */
    getBestMove(board, depth, maximizing = true) {
        // Empty posible moves array
        if (depth === 0) this.nodes = [];

        // check for game completion
        let terminal = board.isTerminal();

        if (terminal || depth == this.maxDepth) {
            if (terminal.winner === this.mark) {
                return 100; // Win
            } else if (terminal.winner !== this.mark && terminal.winner !== 'draw') {
                return -100; // Loss
            }
            // Draw
            return 0;
        }

        // Go through all empty cells
        let emptyCells = board.getEmptyCells();
        //console.log(emptyCells);
        for (let index of emptyCells) {
            let child = new Board(board.getPlayersState());
            // clone board state into child
            child.state = board.state.map(arr => arr.slice());
            child.markCell(index.x, index.y, child.currentPlayer);
            child.nextPlayer();
            let value;
            if (maximizing) {
                value = -100;
                let newValue = this.getBestMove(child, depth + 1, maximizing ? false : true);
                value = Math.max(value, newValue);
                console.log(`MAX | Depth: ${depth} | Coords: ${index.x} ${index.y} | Value: ${value} | Computed: ${newValue}`);
                this.nodes.push({
                    value: value,
                    cell: index
                });
            } else {
                value = 100;
                let newValue = this.getBestMove(child, depth + 1, maximizing ? false : true);
                value = Math.min(value, newValue);
                console.log(`MIN | Depth: ${depth} | Coords: ${index.x} ${index.y} | Value: ${value} | Computed: ${newValue}`);
                this.nodes.push({
                    value: value,
                    cell: index
                });
            }
        }

        // Initial depth so lets return best possible move coords
        if (depth === 0) {
            let best = -1000;
            let bestCell = {};
            this.nodes.forEach(node => {
                if (node.value > best) {
                    best = node.value;
                    bestCell = node.cell;
                }
            });
            return bestCell;
        }
    }
}

export default Player;