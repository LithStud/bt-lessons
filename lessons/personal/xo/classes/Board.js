class Board {
    /**
     * Game Board Class
     * @param {{playerOne: String, playerTwo: String, currentPlayer: String}} players Players object
     * @param {Number} rows Number of rows (default 3)
     * @param {Number} cols Number of columns (default 3)
     * @param {Any} defaultValue default value to represent empty cell (default null)
     */
    constructor(players, rows = 3, cols = 3, defaultValue = null) {
        this.rows = rows;
        this.cols = cols;
        this.emptyValue = defaultValue;
        this.playerOne = players.playerOne;
        this.playerTwo = players.playerTwo;
        this.currentPlayer = players.currentPlayer;

        this.state = this.createBoard();
    }

    /**
     * Creates and returns board state
     */
    createBoard() {
        let board = [];
        for (let i = 0; i < this.rows; i++) {
            board[i] = [];
            for (let j = 0; j < this.cols; j++)
                board[i][j] = this.emptyValue;
        }
        return board;
    }

    /**
     * Returns current players state as well marks used by players
     */
    getPlayersState() {
        return {
            playerOne: this.playerOne,
            playerTwo: this.playerTwo,
            currentPlayer: this.currentPlayer,
        }
    }

    /**
     * Checks for win conditions.
     */
    isTerminal() {
        // check if board is empty
        if (this.isEmpty())
            return false;

        // functions for winner checking
        let that = this;
        function winnerPlayerOne(value) {
            return value === that.playerOne;
        };
        function winnerPlayerTwo(value) {
            return value === that.playerTwo;
        }

        // horizontal lines
        for (let x = 0; x < this.rows; x++) {
            if (this.state[x].every(winnerPlayerOne))
                return { winner: this.playerOne, type: 'row', coords: x };
            if (this.state[x].every(winnerPlayerTwo))
                return { winner: this.playerTwo, type: 'row', coords: x };
        }

        //check vertical lines
        for (let y = 0; y < this.cols; y++) {
            let tempCol = [];
            for (let x = 0; x < this.rows; x++) {
                tempCol.push(this.state[x][y]);
            }
            if (tempCol.every(winnerPlayerOne)) return { winner: this.playerOne, type: 'col', coords: y };
            if (tempCol.every(winnerPlayerTwo)) return { winner: this.playerTwo, type: 'col', coords: y };
        }
        //check diagonals
        let diag1 = [], diag2 = [];
        for (let x = 0; x < this.state.length; x++) {
            diag1.push(this.state[x][x]);
            diag2.push(this.state[x][this.state.length - x - 1]);
        }
        if (diag1.every(winnerPlayerOne)) return { winner: this.playerOne, type: 'diag', coords: 'lr' };
        if (diag1.every(winnerPlayerTwo)) return { winner: this.playerTwo, type: 'diag', coords: 'lr' };

        if (diag2.every(winnerPlayerOne)) return { winner: this.playerOne, type: 'diag', coords: 'rl' };
        if (diag2.every(winnerPlayerTwo)) return { winner: this.playerTwo, type: 'diag', coords: 'rl' };

        // draw
        if (this.isFull())
            return { winner: 'draw' };

        // No winner yet
        return false;
    }
 
    /**
     * Marks one cell (eg. x, o)
     * @param {Number} x Row number
     * @param {Number} y Column number
     * @param {Any} value How to mark cell (defaults to null)
     */
    markCell(x, y, value = null) {
        if (this.state[x][y] === this.emptyValue)
            this.state[x][y] = value;
        else console.log(`Invalid Cell [${x}][${y}] - Its not empty`);
    }

    /**
     * Switches active player
     */
    nextPlayer() {
        if (this.currentPlayer === this.playerOne)
            this.currentPlayer = this.playerTwo;
        else this.currentPlayer = this.playerOne;
    }

    /**
     * Checks if board is empty
     */
    isEmpty() {
        for (let row of this.state) {
            if (!row.every(value => value == this.emptyValue))
                return false;
        }
        return true;
    }

    /**
     * Check if board is full
     */
    isFull() {
        for (let row of this.state) {
            if (!row.every(value => value != this.emptyValue))
                return false;
        }
        return true;
    }

    /**
     * Generates html elements to match board state.
     * Uses markedX and markedO CSS classes to mark cells.
     */
    drawBoard(boardEl) {
        boardEl.classList = [];
        boardEl.classList.add('board', `player${this.currentPlayer}`);
        boardEl.innerHTML = `
            <div class="hdividers"></div>
            <div class="vdividers"></div>
        `;
        this.state.forEach((row, x) => {
            row.forEach((col, y) => {
                let mark = this.state[x][y] === this.playerOne
                    && this.state[x][y] != this.emptyValue ? `marked${this.playerOne}` : `marked${this.playerTwo}`;
                boardEl.innerHTML += `<div class="cell ${this.state[x][y] === this.emptyValue ? 'empty' : mark}" data-x="${x}" data-y="${y}"></div>`;
            });
        });
    }

    /**
     * Returns array of empty cells coordinates
     */
    getEmptyCells() {
        /**
         * @type {[{x: Number, y: Number}]}
         */
        let emptyCells = [];
        this.state.forEach((row, x) => {
            row.forEach((col, y) => {
                if (col === this.emptyValue)
                    emptyCells.push({ x: x, y: y });
            });
        });
        return emptyCells;
    }

    /**
     * Returns random Integer between min and max. The maximum is inclusive and the minimum is inclusive.
     * @param {Number} min minimum number
     * @param {Number} max maximum number
     */
    getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}
export default Board;