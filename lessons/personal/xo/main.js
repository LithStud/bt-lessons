import Board from "./classes/Board.js";
import Player from "./classes/Player.js";

const players = {
    playerOne: 'X',
    playerTwo: 'O',
    currentPlayer: 'X',
};

let boardElem = document.getElementById('board');

// AI Player
let playerTwo = new Player(players.playerTwo, -1);

/**
 * Function to register onto board
 * @param {Event} e Event object
 */
function cellClick(e) {
    if (e.target.classList.contains('empty')) {
        let x = Number(e.target.dataset.x);
        let y = Number(e.target.dataset.y);
        XO.markCell(x, y, XO.currentPlayer);
        XO.drawBoard(boardElem);
        let terminal = XO.isTerminal();
        if (terminal === false) {
            XO.nextPlayer();
            console.log('AI Turn');
            let bestMove = playerTwo.getBestMove(XO, 0, true);
            console.log(bestMove);
            XO.markCell(bestMove.x, bestMove.y, XO.currentPlayer);
            XO.nextPlayer();
            XO.drawBoard(boardElem);
            terminal = XO.isTerminal();
            if (terminal) {
                displayWinners(terminal);
            }
        } else {
            console.log(terminal);
            displayWinners(terminal);
        }
    }
}

/**
 * Registers click event for clicked board cells
 */
boardElem.addEventListener('click', cellClick);

/**
 * Visualize winning, removes event listener from board
 * @param {{winner: String, type: String, coords: Number|String}} terminal Object containing winning results
 */
function displayWinners(terminal) {
    console.log('Cleanup')
    console.log('F', terminal);
    boardElem.removeEventListener('click', cellClick);
    boardElem.classList = [];
    boardElem.classList.add('board');
    switch (terminal.type) {
        case 'row':
            boardElem.querySelectorAll(`.cell[data-x="${terminal.coords}"]`).forEach(el => {
                el.style.backgroundColor = 'green';
            });
            break;

        case 'col':
            boardElem.querySelectorAll(`.cell[data-y="${terminal.coords}"]`).forEach(el => {
                el.style.backgroundColor = 'green';
            });
            break;

        case 'diag':
            boardElem.classList.add(`diagonalWin${terminal.coords.toUpperCase()}`);
            break;

        default:
            break;
    }
}

// New XO game
let XO = new Board(players);

//console.table(XO.state);
//console.log(XO.isTerminal());

//XO.markCell(0, 0, 'x');
//console.table(XO.state);
//console.log(XO.isTerminal());

//XO.markCell(0, 2, XO.currentPlayer);
XO.markCell(0, 0, players.playerOne);
XO.markCell(2, 0, players.playerOne);
XO.markCell(2, 1, players.playerOne);

XO.markCell(0, 2, XO.playerTwo);
XO.markCell(1, 1, XO.playerTwo);
XO.markCell(2, 2, XO.playerTwo);

let AI = new Player(players.playerOne, -1);
console.log(AI.getBestMove(XO, 0, true));

/* XO.nextPlayer();
console.table(XO.state); */
//console.log(XO.isTerminal());
/* let playerTwo = new Player(players.playerTwo, 7);
console.log(playerTwo.getBestMove(XO, 0, -100, 100, true));
console.log(playerTwo.nodes); */
XO.drawBoard(boardElem);

// for debugging only
window.XO = XO;