/**
 * This is Main game file
 * Author: Rimvydas
 * Date: 2019-02
 * Version: 1.0
 */


/**
 * @type {HTMLCanvasElement}
 */
let canvas = UI.canvas;
let ctx = canvas.getContext('2d');

// Frame timing variables
let previousFrameTime;
let deltaTime;

// Solo or Versus
let twoPlayerGame = false;

// Game state
let gameStarted = false;

/**
 * Player One Board
 * @type {Board}
 */
let boardOne = null;
/**
 * Player Two Board
 * @type {Board}
 */
let boardTwo = null;

// Next shape board with its own options object
let nextShapeBoard = new Board('Next Shape', { grid: { rows: 4, cols: 4 }, cell: { w: 20, h: 20 } });

// Shape selection
let selection = ['O', 'I', 'S', 'Z', 'L', 'J', 'T'];

// Predetermined coords for shape to appear on main board
const refCoords = { row: 0, col: 5 };
// Predetermined coords for shape to appear on next shape board
const nextBoardRefCoords = { row: 1, col: 2 }

/**
 * Next Shape to be used
 * @type {Shape}
 */
let nextShape;

/**
 * Setup function to setup initial variables
 */
function setup() {
    // Canvas
    canvas.width = twoPlayerGame ? options.width.versus : options.width.solo;
    canvas.height = options.height;

    // Initial next shape
    nextShape = randomShape(true);

    // Board One
    boardOne = new Board(UI.modeSelect.playerOneName.value, options);
    boardOne.controledShape = randomShape();
    boardOne.generateInitialState();
    boardOne.isPaused = true;

    if (twoPlayerGame) {
        // Board Two
        boardTwo = new Board(UI.modeSelect.playerTwoName.value, options);
        boardTwo.controledShape = randomShape();
        boardTwo.generateInitialState();
        boardTwo.isPaused = true;
    }

    // change game state
    gameStarted = true;
    // Requesting first frame
    previousFrameTime = Date.now();
    window.requestAnimationFrame(frame);
}

/**
 * Main draw function
 */
function draw() {
    // Clear Canvas
    ctx.fillStyle = '#000000';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    // Boards
    ctx.save();
    // Player one
    ctx.translate(20, 40);
    boardOne.drawGrid(ctx, '#666666');
    boardOne.drawState(ctx);
    if (boardOne.gameOver) {
        gameOverText(ctx);
    }

    // Player two
    if (twoPlayerGame) {
        ctx.translate(360, 0);
        boardTwo.drawGrid(ctx, '#666666');
        boardTwo.drawState(ctx);
        if (boardTwo.gameOver) {
            gameOverText(ctx);
        }
    }
    ctx.restore();

    // Player IDs
    ctx.save();
    ctx.translate(20, 40 + options.grid.rows * options.cell.h);
    ctx.fillStyle = '#FFFFFF';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.font = '20px Arial';
    ctx.fillText(boardOne.name, options.grid.cols / 2 * options.cell.w, 20);
    if (twoPlayerGame) {
        ctx.translate(360, 0);
        ctx.fillText(boardTwo.name, options.grid.cols / 2 * options.cell.w, 20);
    }
    ctx.restore();

    // Next Shape Board
    ctx.save();
    ctx.translate(260, 60);
    ctx.fillStyle = '#FFFFFF';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'bottom';
    ctx.font = '16px Arial';
    ctx.fillText('Next Shape:', nextShapeBoard.cols / 2 * options.cell.w, 0);
    //ctx.fillStyle = '#001100';
    nextShapeBoard.drawGrid(ctx, '#FFFFFF');
    nextShape.draw(ctx);

    if (twoPlayerGame) {
        // Show current leader under next shape board
        ctx.translate(0, 200);
        ctx.fillText('Leader:', nextShapeBoard.cols / 2 * options.cell.w, 0);
        ctx.fillText(
            boardOne.score === boardTwo.score ? 'None' :
                boardOne.score > boardTwo.score ? boardOne.name : boardTwo.name,
            nextShapeBoard.cols / 2 * options.cell.w, 20);
    }
    ctx.restore();

    // Hide any shape that would extrude from board top
    ctx.fillRect(0, 0, canvas.width, 39); // -1px to keep constant border width

    // Draws score and level
    ctx.save();
    ctx.translate(20, 0);
    drawScoreLevel(boardOne, ctx);
    if (twoPlayerGame) {
        ctx.translate(360, 0);
        drawScoreLevel(boardTwo, ctx);
    }
    ctx.restore();
}

/**
 * Draws game over over the board
 * @param {CanvasRenderingContext2D} ctx Rendering context
 */
function gameOverText(ctx) {
    let text = 'GAME OVER';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.font = '30px Arial';
    ctx.fillStyle = 'rgba(0,0,0,0.5)';
    ctx.fillRect(0, 0, options.grid.cols * options.cell.w, options.grid.rows * options.cell.h);
    ctx.fillStyle = '#FFFFFF';
    ctx.fillText(text,
        options.grid.cols * options.cell.w / 2,
        options.grid.rows * options.cell.h / 2)
}

/**
 * Draws score and level of the board
 * @param {Board} board Board to display
 * @param {CanvasRenderingContext2D} ctx Canvas context
 */
function drawScoreLevel(board, ctx) {
    ctx.fillStyle = '#FFFFFF';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.font = '20px Arial';
    ctx.fillText(`Level: ${board.level} | Score: ${board.score}`, board.cols / 2 * options.cell.w, 20);
}

/**
 * Update shape movement according to board
 * @param {Board} board Board to use data from
 */
function update(board) {
    let thisTime = Date.now();
    let currentTime = thisTime - board.previousTime;
    // get board timer (when to move)
    let timer = board.getTimer();

    // if board is paused just update previous time to prevent jerking of shapes upon resuming
    // board pasuing also used during line clean up.
    if (board.isPaused) {
        board.previousTime = thisTime - currentTime % timer;
        return;
    }

    if (currentTime >= timer) {
        if (board.controledShape !== null) {
            move(board, 1, 0);
        }
        board.previousTime = thisTime - currentTime % timer;
    }
}

/**
 * Main animation frame function
 */
function frame() {
    if ((twoPlayerGame && boardOne.gameOver && boardTwo.gameOver) || (!twoPlayerGame && boardOne.gameOver)) {
        console.log('Game finished.');
        // draw last state
        draw();
        // call end game function
        gameOver();
        return;
    } else if (!gameStarted) {
        // this case should be reached only when choosing to go back to mode selection from pause menu.
        return;
    }

    window.requestAnimationFrame(frame);
    let thisFrameTime = Date.now();
    deltaTime = thisFrameTime - previousFrameTime;

    // 30 FPS lock
    if (deltaTime > (1000 / 30)) {
        update(boardOne);
        if (twoPlayerGame)
            update(boardTwo);
        draw();
        previousFrameTime = thisFrameTime - (deltaTime % (1000 / 30));
    }
}

/**
 * Generates and returns random shape with random color.
 * @param {Boolean} isNext if true will use refCoords for next shape board
 */
function randomShape(isNext = false) {
    let r, g, b;
    r = Math.floor(Math.random() * 200 + 55);
    g = Math.floor(Math.random() * 200 + 55);
    b = Math.floor(Math.random() * 200 + 55);
    let color = `rgba(${r},${g},${b},1)`;
    let shape = new Shape(
        selection[Math.floor(Math.random() * selection.length)],
        color,
        isNext ? nextBoardRefCoords : refCoords,
        options);

    shape.rotation = Math.floor(Math.random() * shape.getRotationStates().totalStates);
    shape.createShape();
    return shape;
}

/**
 * Moves shape and checks for collisions. Returns false if shape collides.
 * @param {Board} board Board object to be manipulated
 * @param {Number} row how far to go on row
 * @param {Number} col how far to go on col
 */
function move(board, row, col) {
    let moved = board.moveShape(row, col);
    if (!moved && row === 1 && !board.gameOver) {
        // couldnt be moved, means bottom or collision with shape below,
        // board will record current shape into state as part of the move
        // need to assign new shape for board.
        board.controledShape = nextShape;
        // need to overwrite refCorrds that was set for next shape board
        board.controledShape.refCoords = Object.assign({}, refCoords);
        board.controledShape.createShape();
        // create new next shape
        nextShape = randomShape(true);
    }
}

/**
 * Rotates shape, returns false if rotation is imposible
 * @param {Board} board Board object to be manipulated
 */
function rotate(board) {
    return board.rotateShape();
}

/**
 * Drops shape
 * @param {Board} board Board object to be manipulated
 */
function dropShape(board) {
    let droped = board.dropShape();
    if (droped && !board.gameOver) {
        board.controledShape = nextShape;
        // need to overwrite refCorrds that was set for next shape board
        board.controledShape.refCoords = Object.assign({}, refCoords);
        board.controledShape.createShape();
        // create new next shape
        nextShape = randomShape(true);
    }
}

function handleControls(board, key) {
    //console.log(key);
    if (!board.isPaused)
        switch (key) {
            // Player One
            case "KeyA":
                move(board, 0, -1);
                break;
            case "KeyS":
                dropShape(board);
                //move(boardOne, 1, 0);
                break;
            case "KeyD":
                move(board, 0, 1);
                break;
            case "KeyW":
                rotate(board);
                break;

            // Player Two
            case "ArrowLeft":
                move(board, 0, -1);
                break;
            case "ArrowDown":
                dropShape(board);
                //move(boardTwo, 1, 0);
                break;
            case "ArrowRight":
                move(board, 0, 1);
                break;
            case "ArrowUp":
                rotate(board);
                break;

            default:
                break;
        }
}

/**
 * Handles paused game overlay
 */
function pauseOverlay() {
    // pause / unpause boards
    boardOne.isPaused = boardOne.isPaused ? false : true;
    if (twoPlayerGame)
        boardTwo.isPaused = boardTwo.isPaused ? false : true;

    // hide / unhide pause overlay
    UI.pause.pauseOverlay.classList.toggle('hidden');
}

/**
 * Handles game over overlay and its information
 */
function gameOver() {
    // change game state
    gameStarted = false;

    // default value is draw
    UI.gameOver.winner.dataset.won = "draw";

    // Player One
    UI.gameOver.playerOne.getElementsByClassName('name')[0].innerHTML = boardOne.name;
    UI.gameOver.playerOne.getElementsByClassName('score')[0].innerHTML =
        `Final Score: <span>${boardOne.score}</span>`;

    // If two player game alter overlay accordingly
    if (twoPlayerGame) {
        UI.gameOver.winner.classList.remove('hidden');
        UI.gameOver.playerTwo.classList.remove('hidden');
        // Determine winner
        UI.gameOver.winner.innerHTML = `Winner: <span>
        ${boardOne.score === boardTwo.score ? 'Draw' :
                boardOne.score > boardTwo.score ? boardOne.name : boardTwo.name}</span>`;
        // Style according to winning player (playerOne and playerTwo is specific to css)
        UI.gameOver.winner.dataset.won = boardOne.score === boardTwo.score ? 'draw' :
            boardOne.score > boardTwo.score ? 'playerOne' : 'playerTwo';

        // Player Two
        UI.gameOver.playerTwo.getElementsByClassName('name')[0].innerHTML = boardTwo.name;
        UI.gameOver.playerTwo.getElementsByClassName('score')[0].innerHTML =
            `Final Score: <span>${boardTwo.score}</span>`;
    } else {
        UI.gameOver.winner.classList.add('hidden');
        UI.gameOver.playerTwo.classList.add('hidden');
    }
    UI.gameOver.gameOverOverlay.classList.toggle('hidden');
}

/**
 * EVENT LISTENERS SECTION
 */

// possible control keys
const leftKeys = ["KeyA", "KeyW", "KeyD", "KeyS"];
const rightKeys = ["ArrowLeft", "ArrowUp", "ArrowRight", "ArrowDown"];

// Keyboard controls
document.addEventListener('keydown', e => {
    // console.log(e);

    // Check only if game is setup
    if (gameStarted) {
        // controls depends on if its two player game or not
        if (twoPlayerGame && leftKeys.some(key => key === e.code))
            handleControls(boardOne, e.code);
        else if (twoPlayerGame && rightKeys.some(key => key === e.code))
            handleControls(boardTwo, e.code);

        else if (!twoPlayerGame && (leftKeys.some(key => key === e.code) || rightKeys.some(key => key === e.code)))
            handleControls(boardOne, e.code);
        else {
            switch (e.code) {
                case "Escape":
                    pauseOverlay();
                    break;

                default:
                    break;
            }
        }
    }
});

/**
 * GAME MODE SELECTION OVERLAY
 */

// Select all text upon focusing on input
UI.modeSelect.playerOneName.addEventListener('focus', e => {
    UI.modeSelect.playerOneName.select();
});
// Select all text upon focusing on input
UI.modeSelect.playerTwoName.addEventListener('focus', e => {
    UI.modeSelect.playerTwoName.select();
});

// New Game Button
UI.modeSelect.newGameBtn.addEventListener('click', e => {
    // Hide mode selection overlay
    UI.modeSelect.modeSelectOverlay.classList.toggle('hidden');
    if (UI.modeSelect.soloRadio.checked)
        twoPlayerGame = false;
    else
        twoPlayerGame = true;
    // Setup game
    setup();
    // Show pause overlay (boards shold be set to isPaused = true during setup())
    UI.pause.pauseOverlay.classList.toggle('hidden');
});

/**
 * PAUSE GAME OVERLAY
 */

// Restart Game button from Pause overlay
UI.pause.restartBtn.addEventListener('click', e => {
    // Hide pause overlay
    UI.pause.pauseOverlay.classList.toggle('hidden');
    // Setup game (restart)
    setup();
    boardOne.isPaused = false;
    if (twoPlayerGame)
        boardTwo.isPaused = false;
});

// Select Mode button from Pause overlay
UI.pause.modeSelectBtn.addEventListener('click', e => {
    // set game state
    gameStarted = false;

    // Hide pause overlay
    UI.pause.pauseOverlay.classList.toggle('hidden');

    // Show mode selection overlay
    UI.modeSelect.modeSelectOverlay.classList.toggle('hidden');
});

/**
 * GAME OVER OVERLAY
 */

// New Game button from Game Over overlay
UI.gameOver.startNewBtn.addEventListener('click', e => {
    // Hide mode selection overlay
    UI.gameOver.gameOverOverlay.classList.toggle('hidden');
    if (UI.modeSelect.soloRadio.checked)
        twoPlayerGame = false;
    else
        twoPlayerGame = true;
    // Setup game
    setup();
    // Show pause overlay (boards shold be set to isPaused = true during setup())
    UI.pause.pauseOverlay.classList.toggle('hidden');
});

// Select Mode button from Game Over overlay
UI.gameOver.selectModeBtn.addEventListener('click', e => {
    // Hide mode selection overlay
    UI.gameOver.gameOverOverlay.classList.toggle('hidden');

    // Show mode selection overlay
    UI.modeSelect.modeSelectOverlay.classList.toggle('hidden');
});