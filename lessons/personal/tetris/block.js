class Block {
    /**
     * Block class
     * @param {Number} row Curent row coord
     * @param {Number} col Current col coord
     * @param {Boolean} isOrigin Is this block considered as origin for a shape. DEPRECATED
     * @param {options} options GlobalOptions
     */
    constructor(row, col, options, isOrigin = false) {
        this.row = row;
        this.col = col;
        this.width = options.cell.w;
        this.height = options.cell.h;
        this.isOrigin = isOrigin;
    }

    /**
     * Converts from row and column to x,y coordinates and returns object {x, y}
     */
    getXY() {
        return {
            x: this.width * this.col,
            y: this.height * this.row,
        }
    }

    /**
     * Returns object {row, col} with block coordinates by row and column
     */
    getCoords() {
        return {
            row: this.row,
            col: this.col,
        }
    }

    /**
     * Moves block coordinates by supplied row, col steps
     * @param {Number} rowStep row step
     * @param {Number} colStep column step
     */
    move(rowStep, colStep) {
        this.row += rowStep;
        this.col += colStep;
    }

    /**
     * DEPRECATED. Returns blocks collision object.
     */
    getCollisionData() {
        return {
            coords: {
                rows: { min: this.row, max: this.row },
                cols: { min: this.col, max: this.col }
            }
        };
    }

    /**
     * Draw block.
     * @param {CanvasRenderingContext2D} ctx rendering context
     * @param {String} color color to fill block
     */
    draw(ctx, color) {
        ctx.fillStyle = color;
        // convert row,col to x,y
        let coords = this.getXY();
        ctx.fillRect(coords.x, coords.y, this.width, this.height);
        ctx.strokeRect(coords.x, coords.y, this.width, this.height);
    }
}