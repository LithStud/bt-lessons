/**
 * Global Board Options
 */
let options = {
    width: {
        versus: 600,
        solo: 360
    },
    height: 500,
    cell: {
        w: 20,
        h: 20,
    },
    grid: {
        rows: 20,
        cols: 10,
    }
}

/**
 * UI Components
 */
const UI = {
    modeSelect: {
        modeSelectOverlay: document.getElementById('modeSelect'),
        soloRadio: document.getElementById('solo'),
        versusRadio: document.getElementById('versus'),
        playerOneName: document.getElementById('playerOneName'),
        playerTwoName: document.getElementById('playerTwoName'),
        newGameBtn: document.getElementById('newGameBtn'),
    },
    canvas: document.getElementById('canvas'),
    pause: {
        pauseOverlay: document.getElementById('pause'),
        restartBtn: document.getElementById('restartBtn'),
        modeSelectBtn: document.getElementById('modeSelectBtn'),
    },
    gameOver: {
        gameOverOverlay: document.getElementById('gameOver'),
        winner: document.getElementById('winner'),
        playerOne: document.getElementById('playerOne'),
        playerTwo: document.getElementById('playerTwo'),
        startNewBtn: document.getElementById('startNewBtn'),
        selectModeBtn: document.getElementById('selectModeBtn'),
    }
}