class Shape {
    /**
     * Shape class
     * @param {String} type Type of shape to create
     * @param {String} color Shape color
     * @param {{row: Number, col: Number}} refCoords Initial coordinates for shape origin
     * @param {options} options GlobalOptions
     */
    constructor(type, color, refCoords, options) {
        /**
         * @type {Block[]}
         */
        this.blocks = [];
        this.type = type;
        this.color = color;
        this.rotation = 0;
        this.refCoords = Object.assign({}, refCoords);
        this.options = options;
        this.isMoving = false;
        // create shape blocks
        this.createBlocks();
        // assemble blocks by shape rotation rules
        this.createShape();
    }

    /**
     * Creates 4 blocks that will form a shape
     */
    createBlocks() {
        for (let i = 0; i < 4; i++)
            this.blocks.push(new Block(0, 0, this.options));
    }

    /**
     * Assembles blocks by rotation rules
     */
    createShape() {
        // get rotation rules
        let offsets = this.getRotationStates();
        // check for invalid rotation
        if (this.rotation >= offsets.totalStates)
            this.rotation = 0;
        else if (this.rotation < 0)
            this.rotation = offsets.totalStates - 1;
        // assemble shape
        this.blocks.forEach((block, index) => {
            block.row = this.refCoords.row + offsets[this.rotation][index][0];
            block.col = this.refCoords.col + offsets[this.rotation][index][1];
        });
    }

    /**
     * Returns object with rotation rules according to shape type
     */
    getRotationStates() {
        switch (this.type) {
            case 'O':
                return {
                    totalStates: 1,
                    0: [
                        [0, -1], [0, 0],
                        [1, -1], [1, 0]
                    ]
                }
                break;
            case 'I':
                return {
                    totalStates: 2,
                    0: [
                        [0, -2], [0, -1], [0, 0], [0, 1]
                    ],
                    1: [
                        [-1, 0], [0, 0], [1, 0], [2, 0]
                    ]
                }
                break;
            case 'S':
                return {
                    totalStates: 2,
                    0: [
                        [0, 0], [0, 1], [1, -1], [1, 0]
                    ],
                    1: [
                        [-1, 0], [0, 0], [0, 1], [1, 1]
                    ]
                }
                break;
            case 'Z':
                return {
                    totalStates: 2,
                    0: [
                        [0, -1], [0, 0], [1, 0], [1, 1]
                    ],
                    1: [
                        [-1, 1], [0, 0], [0, 1], [1, 0]
                    ]
                }
                break;
            case 'L':
                return {
                    totalStates: 4,
                    0: [
                        [0, -1], [0, 0], [0, 1], [1, -1]
                    ],
                    1: [
                        [-1, 0], [0, 0], [1, 0], [+1, 1]
                    ],
                    2: [
                        [0, -1], [0, 0], [0, 1], [-1, 1]
                    ],
                    3: [
                        [-1, -1], [-1, 0], [0, 0], [1, 0]
                    ]
                }
                break;
            case 'J':
                return {
                    totalStates: 4,
                    0: [
                        [0, -1], [0, 0], [0, 1], [1, 1]
                    ],
                    1: [
                        [-1, 0], [-1, 1], [0, 0], [1, 0]
                    ],
                    2: [
                        [-1, -1], [0, -1], [0, 0], [0, 1]
                    ],
                    3: [
                        [-1, 0], [0, 0], [1, -1], [1, 0]
                    ]
                }
                break;
            case 'T':
                return {
                    totalStates: 4,
                    0: [
                        [0, -1], [0, 0], [0, 1], [1, 0]
                    ],
                    1: [
                        [-1, 0], [0, 0], [0, 1], [1, 0]
                    ],
                    2: [
                        [-1, 0], [0, -1], [0, 0], [0, 1]
                    ],
                    3: [
                        [-1, 0], [0, -1], [0, 0], [1, 0]
                    ]
                }
                break;

            default:
                console.error('Invalid shape type supplied');
                return { totalStates: 0 };
        }
    }

    /**
     * Returns array of all blocks coordinates on the board {row, col}
     */
    getCoords() {
        let data = [];
        for (let block of this.blocks) {
            data.push(block.getCoords());
        }
        return data;
    }

    /**
     * Moves shape blocks by supplied steps and increases its referal coords (center) by same steps
     * @param {Number} rowStep row step
     * @param {Number} colStep column step
     */
    move(rowStep, colStep) {
        this.blocks.map(block => {
            block.row += rowStep;
            block.col += colStep;
        });

        // new center coords
        this.refCoords.row += rowStep;
        this.refCoords.col += colStep;
    }

    /**
     * DEPRECATED: Returns array of collision data for each block in shape.
     */
    getCollisionData() {
        let data = [];
        for (let block of this.blocks) {
            data.push(block.getCollisionData());
        }
        return data;
    }

    /**
     * Increase / Decrease rotation of the shape. Calls createShape() to reasemble blocks by new rotation rules.
     * @param {Boolean} reverse rotate in oposite direction?
     */
    rotate(reverse = false) {
        reverse ? this.rotation-- : this.rotation++;
        // reasemble blocks
        this.createShape();
    }

    /**
     * Draws shape. Calls draw function for each block in shape
     * @param {CanvasRenderingContext2D} ctx 
     */
    draw(ctx) {
        ctx.save();
        ctx.strokeStyle = '#FFFFFF';
        for (let block of this.blocks) {
            block.draw(ctx, this.color);
        }
        ctx.restore();
    }
}