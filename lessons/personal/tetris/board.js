class Board {
    /**
     * Teatris board class (doubles as player as well)
     * @param {String} name Player / Board name
     * @param {options} globalOptions global options object
     */
    constructor(name, globalOptions) {
        this.options = globalOptions;
        this.rows = this.options.grid.rows;
        this.cols = this.options.grid.cols;
        /**
         * 2D Array representing board rows and columns
         * @type {Array[]}
         */
        this.state = [];
        this.score = 0;
        this.level = 1;
        /**
         * Board specific previous time for calculating shape automove times */
        this.previousTime = Date.now();
        this.gameOver = false;
        /**
         * Current active shape on the board
         * @type {Shape}
         */
        this.controledShape = null;
        this.isPaused = false;
        /**
         *  Player name */
        this.name = name;
    }

    /**
     * Generates initial empty 2D array for board state
     */
    generateInitialState() {
        for (let row = 0; row < this.rows; row++) {
            this.state[row] = [];
            for (let col = 0; col < this.cols; col++)
                this.state[row][col] = null;
        }
    }

    /**
     * Returns shape automove times in ms per step.
     * At level 1 - 500ms (min speed)
     * At level 10 - 50ms (max speed)
     */
    getTimer() {
        return ((11 - this.level) * 50);
    }

    /**
     * Draw board grid.
     * @param {CanvasRenderingContext2D} ctx Rendering context
     * @param {String} color Grid color
     */
    drawGrid(ctx, color) {
        ctx.strokeStyle = color;
        ctx.beginPath();
        for (let row = 0; row <= this.rows; row++) {
            ctx.moveTo(0, row * this.options.cell.h);
            ctx.lineTo(this.cols * this.options.cell.w, row * this.options.cell.h);
            for (let col = 0; col <= this.cols; col++) {
                ctx.moveTo(col * this.options.cell.w, 0);
                ctx.lineTo(col * this.options.cell.w, this.rows * this.options.cell.h);
            }
        }
        ctx.stroke();
    }

    /**
     * Draw board state.
     * @param {CanvasRenderingContext2D} ctx Rendering context
     */
    drawState(ctx) {
        ctx.save();
        ctx.strokeStyle = '#FFFFFF';
        for (let row = 0; row < this.rows; row++) {
            for (let col = 0; col < this.cols; col++) {
                // if value is not null means it has a block inside (its color code)
                if (this.state[row][col] !== null) {
                    ctx.fillStyle = this.state[row][col]; // read color
                    // draw block
                    ctx.fillRect(col * this.options.cell.w, row * this.options.cell.h, this.options.cell.w, this.options.cell.h);
                    ctx.strokeRect(col * this.options.cell.w, row * this.options.cell.h, this.options.cell.w, this.options.cell.h);
                }
            }
        }

        // Call shape draw function
        if (this.controledShape !== null)
            this.controledShape.draw(ctx);
        ctx.restore();
    }

    /**
     * Checks if cell in specified row and column index is empty (null).
     * @param {Number} row Row index
     * @param {Number} col Column index
     */
    isCellEmpty(row, col) {
        // check out of bounds
        if (row < 0 || col < 0)
            return false;
        return this.state[row][col] === null ? true : false;
    }

    /**
     * Returns true if supplied shape collides with board borders or its non empty cells
     * @param {Shape} shape shape to check collision with
     */
    isCollision(shape) {
        for (let block of shape.blocks) {
            // block is outside board
            if (block.row < 0 || block.col < 0 || block.row >= this.rows || block.col >= this.cols)
                return true;
            if (!this.isCellEmpty(block.row, block.col))
                return true;
        }
        // no collisions found
        return false;
    }

    /**
     * Deletes full lines and adds new ones at the top of board.
     * Score is increased by amount of full lines found.
     */
    clearFullLines() {
        let fullLines = [];

        // find if there is full line and record its index
        for (let i = 0; i < this.state.length; i++) {
            if (this.state[i].every(val => val !== null)) {
                fullLines.push(i);
            }
        }

        // Delete full lines
        if (fullLines.length > 0) {
            // Increase score by line count
            this.addScore(fullLines.length);
            // removing lines from end of array,
            // this way there shouldnt be any issues with index pointing to wrong line
            for (let i = fullLines.length - 1; i >= 0; i--) {
                this.state.splice(fullLines[i], 1);
            }
            // insert new empty lines at the top of board
            for (let i = 0; i < fullLines.length; i++) {
                let newLine = [];
                for (let j = 0; j < this.cols; j++)
                    newLine[j] = null;
                this.state.unshift(newLine);
            }
            //console.table(this.state);
        }
    }

    /**
     * Adds set amount of points to boards score.
     * @param {Number} value How many points to add to score
     */
    addScore(value) {
        this.score += value;
        this.updateLevel();
    }

    /**
     * Updates level according to tetris rules.
     */
    updateLevel() {
        let earnedLevel;
        if (this.score <= 0) {
            earnedLevel = 1;
        }
        else if ((this.score >= 1) && (this.score <= 90)) {
            earnedLevel = Math.floor(1 + ((this.score - 1) / 10));
        }
        else if (this.score >= 91) {
            earnedLevel = 10;
        }
        this.level = Math.max(1, earnedLevel);
    }

    /**
     * Adds shape to board state, clears full lines and returns true if board is filled to the top
     * @param {Shape} shape Shape to be recorded into board state
     */
    recordShape(shape) {
        //console.log(shape);
        // Record each block of shape excluding those colliding
        for (let block of shape.blocks) {
            if (block.row < 0 || block.col < 0 || block.row >= this.rows || block.col >= this.cols)
                continue;
            if (!this.isCellEmpty(block.row, block.col))
                continue;

            // at this point its legal to change boards cell state to shape color
            this.state[block.row][block.col] = shape.color;
        }

        // Shape has been recorded so no longer needed (new one whould be assigned from outside class)
        this.controledShape = null;

        // delete completed lines
        this.clearFullLines();

        // check if board is filled to the top - if at least one cell isnt null means shape is touching board top.
        if (this.state[0].some(val => val !== null)) {
            this.gameOver = true;
            return true;
        } else {
            return false;
        };
    }

    /**
     * Moves shape and checks for collisions. Returns false if shape hasnt been moved.
     * @param {Number} row how far to go on row
     * @param {Number} col how far to go on col
     */
    moveShape(row, col) {
        if (this.gameOver)
            return false;

        // move shape
        this.controledShape.move(row, col);

        // check for collision and if its colliding return to previous position
        if (this.isCollision(this.controledShape)) {
            this.controledShape.move(-row, -col);

            // if direction was down and failed means shape hit another shape or reached board bottom
            if (row === 1) {
                //console.log('Its bottomed out!');
                this.recordShape(this.controledShape);
            }
            return false;
        }
        return true;
    }

    /**
     * Drops shape down untill it collides with something. Returns false only if board is in game over state.
     */
    dropShape() {
        if (this.gameOver)
            return false;
        this.isPaused = true; // disable automove as we are working on clean up
        while (true) {
            let moved = this.moveShape(1, 0);
            if (!moved) break;
        }
        this.isPaused = false;
        return true;
    }

    /**
     * Rotates controled shape and returns true if rotated or false if failed.
     */
    rotateShape() {
        if (this.gameOver)
            return false;

        // rotate shape assuming all is good
        this.controledShape.rotate();

        // if collision detected rotate back to original position
        if (this.isCollision(this.controledShape)) {
            this.controledShape.rotate(true);
            return false;
        }
        return true;
    }
}