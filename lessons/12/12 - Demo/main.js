let customer = {
    name: 'Saulius Skvernas',
    age: 32,
    height: 150,
    weight: 77
}

// Tornadas ( height > 160 or age >= 18)
if (customer.age >= 18 || customer.height > 160) {
    console.log(`${customer.name} can use Tornado`);
} else if (customer.weight >= 40) {
    console.log(`${customer.name} offer to use Mini Tornado`);
} else {
    console.log(`${customer.name} no Tornado for you`);
}

// Blood River
if (customer.age >= 8 && customer.weight >= 50) {
    console.log(`${customer.name} can use BloodRiver`);
} else {
    console.log(`${customer.name} no acess to BloodRiver`);
}

/**
 * CIKLAI
 */

console.log("while DEMO");
let i = 0;

while (i < 10) {
    console.log(i++);
}
console.log("while DEMO END");


console.log("while DEMO with arrays");
let cars = [
    {
        name: "Audi",
        maxSpeed: 95
    },
    {
        name: "Mitsubishi",
        maxSpeed: 230
    },
];

i = 0;

while (i < cars.length) {
    console.log(cars[i++].name);
}

console.log("while DEMO with arrays END");

console.log("for DEMO");
for (let i = 0; i < 10; i++) {
    console.log(i);
}
console.log("for DEMO END");

console.log("for DEMO with arrays");
for (let i = 0; i < cars.length; i++) {
    if (cars[i].maxSpeed > 100)
        console.log(`${cars[i].name} is rather fast`);
    else
    console.log(`${cars[i].name} is not so fast`);
}
console.log("for DEMO with arrays END");