function sayHello() {
    console.log('Yo!');
}

sayHello();

function sayHello2(name) {
    console.log(`Yo! Sup ${name}.`);
}

sayHello2("Bebras");

function sayHello3(name, isAmerican) {
    if (isAmerican)
        console.log(`Yo! Sup ${name}.`);
    else
        console.log(`Labas ${name}`);
}

sayHello3("Bebras", false);
sayHello3("John", true);

function trianglePerimeter(a, b, c) {
    return a + b + c;
}

console.log(trianglePerimeter(5, 6, 7));

// parasyti trikampiu masyva ir atspausti visu trikampiu perimetra

let triangles = [
    {
        a: 1,
        b: 2,
        c: 3
    },
    {
        a: 1,
        b: 5,
        c: 3
    },
    {
        a: 1,
        b: 2,
        c: 5
    },
    {
        a: 3,
        b: 2,
        c: 3
    },
    {
        a: 8,
        b: 2,
        c: 3
    },
    {
        a: 5,
        b: 5,
        c: 5
    },
    {
        a: 3,
        b: 4,
        c: 5
    },
];

function calculatePerimeter(a, b, c) {
    return a + b + c;
}

/* forEach */
console.clear();

triangles.forEach((triangle, index) => {
    console.log(`${index}: Perimeter = ${calculatePerimeter(triangle.a, triangle.b, triangle.c)}`);
});

/* for of */
console.clear();

for (const triangle of triangles) {
    console.log(`Perimeter = ${calculatePerimeter(triangle.a, triangle.b, triangle.c)}`);
}

/* for in */
console.clear();

for (const index in Object.keys(triangles)) {
    console.log(`${index}: Perimeter = ${calculatePerimeter(triangles[index].a, triangles[index].b, triangles[index].c)}`);
}

/* for */
console.clear();

for (let i = 0; i < triangles.length; i++) {
    console.log(`${i}: Perimeter = ${calculatePerimeter(triangles[i].a, triangles[i].b, triangles[i].c)}`);
}

/* while */
console.clear();

let i = 0;
while (i < triangles.length) {
    console.log(`${i}: Perimeter = ${calculatePerimeter(triangles[i].a, triangles[i].b, triangles[i].c)}`);
    i++;
}

// Parasyti funkcija kuri grazina true arba false ar perimetras lyginis. Funkcijai paduodamos krastines
console.clear();

function isEqual(a, b, c) {
    return trianglePerimeter(a, b, c) % 2 === 0;
}

triangles.forEach((triangle, index) => {
    let a = triangle.a;
    let b = triangle.b;
    let c = triangle.c;

    if (!isTriangle(a, b, c)) {
        console.log(`Toks neegzistuoja`);
    }

    if (isPitagor(a, b, c)) {
        console.log(`Pitagoras nemelavo, trikampis statusis`);
    }

    console.log(`${index}: Perimeter = ${calculatePerimeter(a, b, c)}`);
    if (isEqual(a, b, c))
        console.log('Triangle perimeter is equal');
    else
        console.log('Triangle perimeter is not equal');

    console.log('===================');
});


// Parasyti funkcija, i kuria padavus trikampio krastines jipasakytu ar toks trikampis egzistuoja.

function isTriangle(a, b, c) {
    return (a + b > c && a + c > b && b + c > a)
}

// prasyti funkcija kuri pasako ar trikampis yra statusis

function isPitagor(a, b, c) {
    return (
        a * a + b * b === c * c || a * a + c * c === b * b || b * b + c * c === a * a);
}

/* console.log(isPitagor(4,3,5)); // true
console.log(isPitagor(4,4,5)); // false */
console.clear();

let planeta = {
    name: 'Earth',
    radius: 6371, //km
    gyvenama: true,
    printData() {
        console.log(`Pavadinimas: ${this.name}`);
        console.log(`Spindulys: ${this.radius} km`);
        console.log(`Gyvenama: ${this.gyvenama ? 'Taip' : 'Ne'}`);
        console.log(`Planetos skersmuo: ${this.getDiameter()} km`);
        console.log('==========================');
    },
    addRadius(amount) {
        this.radius += amount;
    },
    getDiameter() {
        return this.radius * 2;
    }
}

planeta.printData();
planeta.addRadius(100);
planeta.printData();