let rectangles = [];

document.addEventListener('DOMContentLoaded', (e) => {
    console.log('DOM Loaded');

    // Hook up to ADD button
    document.getElementById('add_btn')
        .addEventListener('click', (e) => {
            addRectangle();
            showRectangles();
        });

    // Hook up to Random Fill button
    document.getElementById('random_btn')
        .addEventListener('click', (e) => {
            fillRandomValues();
        });

    // Hook up to Clear button
    document.getElementById('clear_btn')
        .addEventListener('click', (e) => {
            clearInputValues();
        });

    // Hook up to Delete buttons
    document.addEventListener('click', (e) => {
        if (e.target.classList.contains('del_button')) {
            removeRectangle(e.target.dataset.index);
            showRectangles();
        }
    });

    // Show initial results table (should show empty)
    showRectangles();
});


/**
 * Displays rectangles array in HTML
 */
function showRectangles() {
    let resultsElem = document.getElementById('results');
    let largest = null;

    // Clear existing html results (just for Task 12 requirements)
    resultsElem.innerHTML = '';

    // If array is empty notify user and terminate function
    if (rectangles.length < 1) {
        resultsElem.innerHTML = `
            <tr>
                <td colspan="7" class="empty">Nothing to show yet.</td>
            </tr>
        `;
        showLargestHTML(largest);
        return;
    }

    // Show all entries in table with checks required by task 12
    rectangles.forEach((rec, index) => {
        let errors = 0;
        let htmlResult = `
            <tr>
                <td>${index}</td>
        `;

        // Check if width is valid
        if (rec.width <= 0 || isNaN(rec.width)) {
            htmlResult += `<td class="invalid">INVALID (${rec.width})</td>`;
            errors++;
        } else {
            htmlResult += `<td>${rec.width}</td>`;
        }

        // Check if height is valid
        if (rec.height <= 0 || isNaN(rec.height)) {
            htmlResult += `<td class="invalid">INVALID (${rec.height})</td>`;
            errors++;
        } else {
            htmlResult += `<td>${rec.height}</td>`;
        }

        // If there was invalid width or height report to user, otherwise request calculations and display in html
        if (errors > 0) {
            htmlResult += `<td colspan="3" class="invalid">Rectangle is not valid</td>`;
        } else {
            let recValues = calcRectangle(rec.width, rec.height);
            if (largest === null) {
                largest = index;
            } else if (isLarger(recValues.a, area(rectangles[largest].width, rectangles[largest].height))) {
                largest = index;
            }
            htmlResult += `
                <td>${recValues.p}</td>
                <td>${recValues.a}</td>
                <td>${recValues.d}</td>
            `;
        }
        htmlResult += `
            <td><img src="img/trash.svg" alt="Delete rectangle" class="del_button" data-index="${index}"></td>
            </tr>
        `;
        resultsElem.innerHTML += htmlResult;
    });
    showLargestHTML(largest);
}

/**
 * Displays largest results by supplied index, if index equals null tells user there is none
 */
function showLargestHTML(index) {
    largestElem = document.getElementById('largest__result');
    if (index >= rectangles.length)
        index = null;

    if (index === null) {
        largestElem.innerHTML = `
            <td colspan="6" class="empty">Nothing to show yet.</td>
        `;
        return;
    }

    let recValues = calcRectangle(rectangles[index].width, rectangles[index].height);
    largestElem.innerHTML = `
        <td>${index}</td>
        <td>${rectangles[index].width}</td>
        <td>${rectangles[index].height}</td>
        <td>${recValues.p}</td>
        <td>${recValues.a}</td>
        <td>${recValues.d}</td>
    `;
}

/**
 * If first rectangle area is bigger than second returns true, otherwise false
 */
function isLarger(primaryArea, secondaryArea) {
    return primaryArea > secondaryArea;
}

/**
 * Return object with calculated perimeter, area and diagonal
 * {p: number, a: number, d: number}
 */
function calcRectangle(width, height) {
    return {
        p: perimeter(width, height),
        a: area(width, height),
        d: diagonal(width, height)
    }
}

function perimeter(width, height) {
    return width * 2 + height * 2;
}

function area(width, height) {
    return width * height;
}

function diagonal(width, height) {
    return Math.sqrt(width * width + height * height).toFixed(2);
}

/**
 * Adds Width and Height from HTML inputs to rectangles array
 */
function addRectangle() {
    let width = Number(document.getElementById('width').value);
    let height = Number(document.getElementById('height').value);

    rectangles.push({
        width: width,
        height: height
    });
}

/**
 * Removes rectangle from array by its index
 */
function removeRectangle(index) {
    rectangles.splice(index, 1);
}

/**
 * Clears Inputs values
 */
function clearInputValues() {
    document.getElementById('width').value = '';
    document.getElementById('height').value = '';
}

/**
 * Fills in random values for Width and Height
 */
function fillRandomValues() {
    let wElem = document.getElementById('width');
    let hElem = document.getElementById('height');

    wElem.value = getRandomInt(1, 2000);
    hElem.value = getRandomInt(1, 2000);
}

/**
 * Return random value between supplied min and max
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (Math.floor(max) - Math.ceil(min) + 1)) + Math.ceil(min);
}