// available pokemons array
const pokemons = [
    {
        id: 1, // also used for svg file name
        name: 'Bulbasaur',
        hp: 45,
        attack: 49,
        defense: 49,
        skills: [
            {
                name: 'Tackle',
                minLevel: 1,
                power: 40
            },
            {
                name: 'Vine Whip',
                minLevel: 5,
                power: 45
            },
        ]
    },
    {
        id: 4,
        name: 'Charmander',
        hp: 39,
        attack: 52,
        defense: 43,
        skills: [
            {
                name: 'Scratch',
                minLevel: 1,
                power: 40
            },
        ]
    },
    {
        id: 7,
        name: 'Squirtle',
        hp: 44,
        attack: 48,
        defense: 65,
        skills: [
            {
                name: 'Tackle',
                minLevel: 1,
                power: 40
            },
            {
                name: 'Bubble',
                minLevel: 9,
                power: 40
            },
        ]
    },
    {
        id: 10,
        name: 'Caterpie',
        hp: 45,
        attack: 30,
        defense: 35,
        skills: [
            {
                name: 'Tackle',
                minLevel: 1,
                power: 40
            },
        ]
    },
    {
        id: 13,
        name: 'Weedle',
        hp: 40,
        attack: 35,
        defense: 30,
        skills: [
            {
                name: 'Poison Sting',
                minLevel: 1,
                power: 15
            },
        ]
    },
    {
        id: 16,
        name: 'Pidgey',
        hp: 40,
        attack: 45,
        defense: 40,
        skills: [
            {
                name: 'Tackle',
                minLevel: 1,
                power: 40
            },
            {
                name: '	Gust',
                minLevel: 5,
                power: 40
            },
        ]
    },
    {
        id: 19,
        name: 'Rattata',
        hp: 30,
        attack: 56,
        defense: 35,
        skills: [
            {
                name: 'Tackle',
                minLevel: 1,
                power: 40
            },
            {
                name: 'Quick Attack',
                minLevel: 6,
                power: 40
            },
        ]
    },
    {
        id: 21,
        name: 'Spearow',
        hp: 40,
        attack: 60,
        defense: 30,
        skills: [
            {
                name: 'Peck',
                minLevel: 1,
                power: 35
            },
        ]
    },
    {
        id: 23,
        name: 'Ekans',
        hp: 35,
        attack: 60,
        defense: 44,
        skills: [
            {
                name: 'Poison Sting',
                minLevel: 1,
                power: 15
            },
            {
                name: 'Wrap',
                minLevel: 1,
                power: 15
            },
            {
                name: 'Acid',
                minLevel: 8,
                power: 40
            },
        ]
    },
    {
        id: 25,
        name: 'Pikachu',
        hp: 35,
        attack: 55,
        defense: 40,
        skills: [
            {
                name: 'Thunder Shock',
                minLevel: 1,
                power: 40
            },
            {
                name: 'Quick Attack',
                minLevel: 6,
                power: 40
            },
            {
                name: 'Double Kick',
                minLevel: 6,
                power: 30
            },
        ]
    },
];