class Skill {
    /**
     * Skill Object
     * @param {String} name Skill name
     * @param {Number} minLevel Minimum level required for skill
     * @param {Number} power Skill power
     */
    constructor(name, minLevel, power){
        this.name = name;
        this.minLevel = minLevel;
        this.power = power;
    }

    canBeUsed(level) {
        return level >= this.minLevel;
    }
}

class Pokemon {
    /**
     * Pokemon constructor
     * @param {String} name Pokemon name
     * @param {Number} hp Pokemon HP
     * @param {Number} attack Pokemon attack
     * @param {Number} defense Pokemon defense
     * @param {Skill[]} skills Pokemon defense
     * @param {Number} id Pokedex ID (also used for svg filename)
     */
    constructor(name, hp, attack, defense, skills = [], id) {
        this.level = 1;
        this.name = name;
        this.maxHp = hp;
        this.hp = hp;
        this.attack = attack;
        this.defense = defense;
        this.id = id;
        this.skills = skills;
    }

    /**
     * Prints pokemon information to console
     */
    printData() {
        console.log('======[ Pokemon Info ]======');
        console.log(`Pokedex ID: ${this.id}`);
        console.log(`Name: ${this.name}`);
        console.log(`HP: ${this.hp} / ${this.maxHp}`);
        console.log(`Power: ${this.attack}`);
        console.log(`Defense: ${this.defense}%`);
        console.log('============================');
    }

    /**
     * Return current HP, if percent argument is set as true it will return current HP as percent
     * from total HP.
     * @param {Boolean} percent is output needed as percent of total HP
     */
    getHp(percent = false) {
        return percent ? Math.max(0, Math.round(100 * this.hp / this.maxHp)) : this.hp;
    }

    /**
     * Hits supplied enemy pokemon, if skill not supplied it assumes to use random available skill
     * @param {Pokemon} enemy Enemy Pokemon
     * @param {Number} skillIndex Skill index in pokemon skills array
     */
    hit(enemy, modifier = 1, skillIndex = null) {
        // temp workaround
        skillIndex = skillIndex === null ? 0 : skillIndex;
        let damage = (Math.floor(Math.floor(Math.floor(2 * this.level / 5 + 2) * this.skills[skillIndex].power * this.attack / enemy.defense) / 50) + 2) * modifier;
        enemy.hp -= damage;
        console.log(`${this.name} hit ${enemy.name} for ${damage}`);
    }
}