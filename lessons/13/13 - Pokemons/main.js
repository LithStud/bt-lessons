// pokemon svg files directory
const pokFolder = 'pokemons/';

// player and enemy global entitys
/**
 * @type {Pokemon}
 */
let player = null;
/**
 * @type {Pokemon}
 */
let enemy = null;

document.addEventListener('DOMContentLoaded', (e) => {

    // create pokemon selection html
    showPokemonSelection();

    // future hooks for pokemon selector
    document.getElementById('pokemon_selector').addEventListener('click', e => {
        if (e.target.classList.contains('pokemon')) {
            let pokeIndex = Number(e.target.dataset.index);
            let pokemon = pokemons[pokeIndex];
            let badPokemon = pokemons[getRandomInt(0, pokemons.length - 1)];
            console.log('********************');
            console.log(`Player selected pokemon ID: ${pokemon.id}`);
            console.log('Creating selected pokemon');
            player = new Pokemon(pokemon.name, pokemon.hp, pokemon.attack, pokemon.defense, pokemon.skills, pokemon.id);
            player.level = 50;
            player.printData();
            console.log(`Randomly selected enemy pokemon ID: ${badPokemon.id}`);
            enemy = new Pokemon(badPokemon.name, badPokemon.hp, badPokemon.attack, badPokemon.defense, badPokemon.skills, badPokemon.id);
            enemy.level = 25;
            enemy.printData();
            console.log('********************');

            // update battlefield and hide selection window
            updateBatlefieldInfo();
            document.getElementById('overlay').classList.add('hidden');
        }
    });

    // Hit action button
    document.getElementById('combat_moves').addEventListener('click', e => {
        if (e.target.classList.contains('skillBtn')) {
            if (enemy.hp <= 0 || player.hp <= 0) {
                console.log('I see dead pokemons');
                return;
            }
            let skillIndex = Number(e.target.dataset.skill);
            player.hit(enemy, getRandomInt(85, 100) / 100, skillIndex);
            enemy.hit(player, getRandomInt(85, 100) / 100);

            updateBatlefieldInfo();
        }
    });
});

/**
 * Creates Pokemon selector HTML and inserts it into app.
 */
function showPokemonSelection() {
    let selectorElem = document.getElementById('pokemon_selector');
    pokemons.forEach((pokemon, index) => {
        let html = `
            <div class="pokemon" data-index="${index}">
                <img src="${pokFolder + pokemon.id}.svg" alt="${pokemon.name} - Pokemon">
            </div>
        `;
        selectorElem.innerHTML += html;
    });
}

/**
 * Updates battlefield HTML to match both player and enemy pokemons states
 */
function updateBatlefieldInfo() {
    // hooks for enemy based base HTML elements
    let enemyBaseInfoElem = document.getElementById('enemy_info');
    let enemyBaseMonsterElem = document.getElementById('enemy_monster');

    // hooks for player based base HTML elements
    let playerBaseInfoElem = document.getElementById('player_info');
    let playerBaseMonsterElem = document.getElementById('player_monster');

    // update player info
    playerBaseInfoElem.getElementsByClassName('name')[0].innerHTML = player.name;
    playerBaseInfoElem.getElementsByClassName('level')[0].innerHTML = player.level;
    playerBaseInfoElem.getElementsByClassName('bar')[0].style.width = `${player.getHp(true)}%`;
    playerBaseMonsterElem.getElementsByClassName('model')[0].src = `${pokFolder + player.id}.svg`;
    if (player.hp <= 0) {
        console.log('Player dead');
        playerBaseMonsterElem.getElementsByClassName('model')[0].style.filter = 'saturate(0)';
    }

    // update enemy info
    enemyBaseInfoElem.getElementsByClassName('name')[0].innerHTML = enemy.name;
    enemyBaseInfoElem.getElementsByClassName('level')[0].innerHTML = enemy.level;
    enemyBaseInfoElem.getElementsByClassName('bar')[0].style.width = `${enemy.getHp(true)}%`;
    enemyBaseMonsterElem.getElementsByClassName('model')[0].src = `${pokFolder + enemy.id}.svg`;
    if (enemy.hp <= 0) {
        console.log('Enemy dead');
        enemyBaseMonsterElem.getElementsByClassName('model')[0].style.filter = 'saturate(0)';
    }

    // update controls HTML
    updateControls();
}

function updateControls() {
    let playerCombatMovesElem = document.getElementById('combat_moves');
    playerCombatMovesElem.innerHTML = '';
    player.skills.forEach((skill, index) => {
        playerCombatMovesElem.innerHTML += `<button class="skillBtn" data-skill="${index}">${skill.name}</button>`;
    });
}

/**
 * Returns random Integer between min and max. The maximum is inclusive and the minimum is inclusive.
 * @param {Number} min minimum number
 * @param {Number} max maximum number
 */
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}