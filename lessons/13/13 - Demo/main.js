class Pokemon {
    constructor(name, power, hp) {
        this.name = name;
        this.hp = hp;
        this.power = power;
        this.level = 1;
        // console.log('Object Created');
    }

    printData() {
        console.log('======[ Pokemon Info ]======');
        console.log(`Name: ${this.name}`);
        console.log(`Power: ${this.power}`);
        console.log(`HP: ${this.hp}`);
        console.log('============================');
    }

    addHp(value) {
        this.hp += value;
    }

    fight(enemy) {
        let randomEnemyPower = Math.round(enemy.power * Math.random());
        let randomThisPower = Math.round(this.power * Math.random());

        this.hp -= randomEnemyPower;
        this.hp = Math.max(0, this.hp);
        console.log(`${enemy.name} hits ${this.name} with power of ${randomEnemyPower}. ${this.name} has ${this.hp} left.`);

        enemy.hp -= randomThisPower;
        enemy.hp = Math.max(0, enemy.hp);
        console.log(`${this.name} hits ${enemy.name} with power of ${randomThisPower}. ${enemy.name} has ${enemy.hp} left.`);

        console.log('---------------');
    }
}

let pokemon = new Pokemon('Pikachu', 40, 100);
let pokemon2 = new Pokemon('Squirtle', 30, 130);

pokemon.printData();
pokemon.addHp(5);
pokemon.printData();

pokemon2.printData();

while (pokemon.hp > 0 && pokemon2.hp > 0) {
    pokemon.fight(pokemon2);
}