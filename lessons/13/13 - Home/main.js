const goodAverage = 7;
let students = [];

/**
 * Demo students function, adds demo studends into array
 */
function demoStudends() {
    let student = new Student('Testas', 'Testulevic');
    student.setRequiredAverage(goodAverage);
    student.addMark(10);
    student.addMark(5);
    student.addMark(1);
    student.addMark(1);
    student.addMark(1);
    student.setRequiredAverage(1);
    student.printToConsole();

    let student2 = new Student('Testas2', 'Testulevic2', [8, 8, 6], goodAverage);
    student2.printToConsole();

    student.whoIsBetter(student2);
    console.log("**************************************");

    students.push(student, student2);
}

document.addEventListener('DOMContentLoaded', (e) => {
    console.log('DOM Loaded');

    // Add demo studends into array
    demoStudends();
    showStudents();

    // Hook up to ADD STUDENT link
    document.getElementById('add_stud_link')
        .addEventListener('click', (e) => {
            e.preventDefault();
            showHideElementsByClassName('overlay');
        });

    // Hook up to ADD STUDENT button
    document.getElementById('add_student_btn')
        .addEventListener('click', (e) => {
            addStudent();
            showHideElementsByClassName('overlay');
            showStudents();
        });
});

/**
 * Toggles 'hidden' css class on first found element matching supplied css class
 * @param {string} className css class name
 */
function showHideElementsByClassName(className) {
    document.getElementsByClassName(className)[0].classList.toggle('hidden');
}

/**
 * Creates studend and pushes into global studends array
 */
function addStudent() {
    let name = document.getElementById('stud_name').value;
    let surname = document.getElementById('stud_surname').value;
    let marksString = document.getElementById('stud_marks').value;
    let requiredAvg = document.getElementById('req_avg').value;

    students.push(
        new Student(
            name,
            surname,
            stringToArray(marksString),
            requiredAvg
        )
    );
}

/**
 * Accepts string of numbers separated by spaces and returns numbers array (no error checking)
 * @param {string} string numbers separated by spaces
 */
function stringToArray(string) {
    return string.split(' ').map(Number);
}

function showStudents() {
    let studListElem = document.getElementById('stud_list');

    // clear current list
    studListElem.innerHTML = '';

    students.forEach((stud, index) => {
        let newLine = document.createElement('tr');
        let data = `
            <td>${index}</td>
            <td>${stud.name}</td>
            <td>${stud.surname}</td>
            <td>${stud.getAverage()}</td>
            <td>(${stud.goodAverage}) - ${stud.isGoodStudent() ? 'YES' : 'NO'}</td>
        `;
        newLine.innerHTML = data;
        studListElem.appendChild(newLine);
    });
}