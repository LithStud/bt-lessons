class Student {

    /**
     * Student class constructor
     * @param {String} name Student name
     * @param {String} surname Student surname
     * @param {Number[]} marks Studends marks array
     * @param {Number} requiredMark above average student indicator
     */
    constructor(name, surname, marks = [], requiredMark = 0) {
        this.name = name;
        this.surname = surname;
        this.marks = marks;
        this.goodAverage = requiredMark;
    }

    /**
     * returns students marks average (fixed to 2 numbers after dot)
     */
    getAverage() {
        let avg = 0;
        if (this.marks.length < 1)
            return avg;
        Math.round()
        return Number((this.marks.reduce((avg, mark) => avg + mark) / this.marks.length).toFixed(2));
    }

    /**
     * Pushes new mark into studends marks array (no error checking)
     * @param {Number} mark Student earned mark
     */
    addMark(mark) {
        this.marks.push(mark);
    }

    /**
     * Sets minimum required average to be considered as good student
     * @param {Number} avg number to find above average students
     */
    setRequiredAverage(avg) {
        this.goodAverage = avg;
    }

    /**
     * Returns true if studends marks average is above or equal to set limit
     * @param {Number} markAvg Marks average
     */
    isGoodStudent(markAvg = null) {
        return markAvg === null ? this.getAverage() >= this.goodAverage : markAvg >= this.goodAverage;
    }

    /**
     * Compares this student agains supplied one and prints whos avg is better.
     * @param {Student} oposingStudent another Student object to compare with
     */
    whoIsBetter(oposingStudent) {
        let myAvg = this.getAverage();
        let oposingAvg = oposingStudent.getAverage();
        if (myAvg > oposingAvg)
            console.log(`${this.name} avg is better than ${oposingStudent.name}`);
        else if (myAvg === oposingAvg)
            console.log(`${this.name} avg is equal with ${oposingStudent.name}`);
        else
            console.log(`${this.name} avg is worse than ${oposingStudent.name}`);
    }

    /**
     * Prints to console Student information
     */
    printToConsole() {
        let avg = this.getAverage();
        console.log(`Required Average: ${this.goodAverage}`);
        console.log(`${this.name} ${this.surname[0]}. Vidurkis: ${avg}`);
        if (this.isGoodStudent(avg))
            console.log('šaunuolis!');
        console.log("=================");
    }
}