/**
 * @type {HTMLCanvasElement}
 */
let canvas = document.getElementById('canvas');
let ctx = canvas.getContext('2d');
let previousFrameTime;
let deltaTime;

let circle = {
    x: 100,
    y: 100,
    r: 100,
    gameOver: false,

    update() {
        this.r -= 50 * deltaTime / 1000;
        if (this.r < 0) {
            this.gameOver = true;
            this.r = 0;
        }
    },

    collidesPoint(x, y) {
        let xSquare = Math.pow(x - this.x, 2);
        let ySquare = Math.pow(y - this.y, 2);
        return Math.sqrt(xSquare + ySquare) < this.r ? true : false;
    },

    init() {
        this.x = Math.round(Math.random() * 300 + 100);
        this.y = Math.round(Math.random() * 300 + 100);
        this.r = Math.round(Math.random() * 50 + 50);
    }
}

function draw() {
    //Clearing frame
    ctx.clearRect(0, 0, 500, 500);
    if (circle.gameOver) {
        ctx.fillStyle = '#FF0000';
        ctx.font = '50px Monospace';
        ctx.fillText('GAME OVER!', 200, 200);
    } else {
        // Drawing circle
        ctx.beginPath();
        ctx.arc(circle.x, circle.y, circle.r, 0, 2 * Math.PI);
        ctx.stroke();
        ctx.closePath();
    }

    //ctx.drawImage(image)
}

function update() {
    circle.update();
}

function frame() {
    let thisFrameTime = Date.now();
    deltaTime = thisFrameTime - previousFrameTime;

    update();
    draw();

    previousFrameTime = thisFrameTime;
    window.requestAnimationFrame(frame);
}

// Listening for clicks
canvas.addEventListener('click', e => {
    let x = e.layerX;
    let y = e.layerY;

    if (circle.collidesPoint(x, y))
        circle.init();
});

// Requesting first frame
previousFrameTime = Date.now();
window.requestAnimationFrame(frame);