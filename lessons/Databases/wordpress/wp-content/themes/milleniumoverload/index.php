<?php get_header(); ?>

    <div class="wrapper">
        <?php 
            get_template_part('parts/nav');
            get_template_part('loop');
        ?>

        <div class="paginator">
            <!-- <div class="page active">1</div>
            <div class="page">2</div>
            <div class="page">3</div> -->

            <?php echo paginate_links(); ?>
        </div>

    </div>

<?php get_footer(); ?>