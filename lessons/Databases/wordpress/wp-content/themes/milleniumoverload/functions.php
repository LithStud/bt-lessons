<?php
    register_nav_menu('top_menu', 'Top Main Menu');
    register_nav_menu('footer_menu', 'Footer Menu');

    add_theme_support('post-thumbnails');

    function musu_customizer($wp_customize) {
        // Create section
        $wp_customize->add_section('footer_settings', array(
            'title' => 'Footer Settings',
            'priority' => 20
        ));

        // Adding settings for footer text
        $wp_customize->add_setting('footer_text', array(
            'default' => 'Lorem Ipsum',
            'transport' => 'refresh'
        ));
        // Adding settings for footer logo
        $wp_customize->add_setting('footer_logo', array(
            'transport' => 'refresh'
        ));

        // Adding settings for footer color
        $wp_customize->add_setting('footer_color', array(
            'default' => '#5C6BC0',
            'transport' => 'refresh'
        ));

        // Adding controls
        $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_text', array(
            'label' => 'Footer Text',
            'section' => 'footer_settings',
            'settings' => 'footer_text'
        )));

        // Adding color controls
        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_color', array(
            'label' => 'Footer Color',
            'section' => 'footer_settings',
            'settings' => 'footer_color'
        )));
        // Adding logo controls
        $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'footer_logo', array(
            'label' => 'Footer Logo',
            'section' => 'footer_settings',
            'settings' => 'footer_logo'
        )));
    }

    add_action('customize_register', 'musu_customizer');
?>