<?php 
if ( have_posts() ) {
	while ( have_posts() ) {
        the_post();
        ?>
        <div class="inner post">
            <div class="header">
                <?php if (has_post_thumbnail()) { ?>
                <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                <?php } ?>
            </div>
            <main>
                <div class="category"><?php the_category(); ?></div>
                <div class="date"><?php the_date('Y.m.d'); ?></div>
                <h2><?php echo get_the_title(); ?></h2>
                <?php the_excerpt(); ?>
                <div class="footer">
                    <a href="<?php the_permalink(); ?>">read more</a>
                    <div class="tags">
                        <?php the_tags(); ?>
                    </div>
                </div>
            </main>
        </div>
        <?php
	} // end while
} // end if
?>