<?php get_header(); ?>
<div class="wrapper">
    <?php get_template_part('parts/nav'); ?>

    <?php
        if (have_posts()) {
            the_post();
            ?>
            <div class="inner post">
            <div class="header">
                <?php if (has_post_thumbnail()) { ?>
                <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                <?php } ?>
            </div>
            <main>
                <h2><?php echo get_the_title(); ?></h2>
                <?php the_content(); ?>
            </main>
            </div>
            <?php
        }
    ?>
</div>
<?php get_footer(); ?>