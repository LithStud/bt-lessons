    <style>
        footer {
            background-color: <?php echo get_theme_mod('footer_color'); ?>
        }
    </style>

    <footer>
        <div class="inner">
            <img src="<?php echo get_theme_mod('footer_logo'); ?>" alt="">
            <?php wp_nav_menu(array('theme_location' => 'footer_menu')); ?>
            <p><?php echo get_theme_mod('footer_text'); ?></p>
            <div class="copyright">© 2015 - Palo Alto. All Rights Reserved. Designed & Developed by <span>PixelBuddha
                    Team</span></div>
            <div class="social">
                <img src="<?php echo get_template_directory_uri(); ?>/img/fb.svg" alt="Facebook">
                <img src="<?php echo get_template_directory_uri(); ?>/img/twitter.svg" alt="Twitter">
                <img src="<?php echo get_template_directory_uri(); ?>/img/instagram.svg" alt="Instagram">
                <img src="<?php echo get_template_directory_uri(); ?>/img/pinterest.svg" alt="Pinterest">
            </div>
        </div>
    </footer>
</body>

</html>