// Uzduotis 1
$('.sita-slept').on('click', function (e) {
    console.log(this);
    $(this).hide();
});

// Uzduotis 2
$('.green').on('click', function (e) {
    $(this).css('background', 'green');
});

// Uzduotis 3
$('.forma button').on('click', function (e) {
    console.log($('.inputas').val());
    let value = $('.inputas').val();
    // Uzduotis 4.1 + 4.2
    $('.sub').append(`<div class="submenu">${value} <span class="delete" data-temp="">X</span></div>`);
});

// Uzduotis 4.2
$(document).on('click', '.delete[data-temp]', function (e) {
    $(this).parent().remove();
});

// Uzduotis 4
$('.top').on('click', function (e) {
    $(this).toggleClass('hidden');
});