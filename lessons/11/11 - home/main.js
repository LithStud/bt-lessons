const maxSpeed = 60;

const carModels = ['Audi', 'BMW', 'Lambo', 'Moskwich', 'WV', 'Ford'];
var carDB = [
    {
        model: "Audi",
        traveled: 2000, // km
        travelTime: 24, // h
        licPlate: "AAA-666",
    },
    {
        model: "BMW",
        traveled: 100, // km
        travelTime: 2, // h
        licPlate: "BBB-777",
    },
];

document.addEventListener('DOMContentLoaded', (e) => {
    console.log('DOM Loaded');

    calculate();

    document.getElementById('add_btn')
        .addEventListener('click', (e) => {
            e.preventDefault();
            addCar();
        });

    document.getElementById('random_btn')
        .addEventListener('click', (e) => {
            e.preventDefault();
            fillRandomValues();
        });
});

function calculate() {
    carDB.forEach((car, index) => {
        let avgSpeed = getAvgSpeed(car.traveled, car.travelTime);
        console.log(`========[ ${index} ]========`);
        console.log(car);
        console.log('Car Avg. Speed: ' + avgSpeed);
        if (avgSpeed > 60)
            console.log('Speeding ticket! - ' + car.licPlate);
        console.log(`============================`);
        showInHTML(car);
    });
    /* for (let index = 0; index < carDB.length; index++) {
        showInHTML(carDB[index]);
    } */
}

function showInHTML(car) {
    let avgSpeed = getAvgSpeed(car.traveled, car.travelTime);
    let resultsElem = document.getElementById('results');
    let newHtmlElem = document.createElement('tr');
    let ticket = avgSpeed > 60 ? 'yes' : 'no';
    newHtmlElem.innerHTML = `
        <td>${car.model}</td>
        <td>${car.licPlate}</td>
        <td>${car.traveled}</td>
        <td>${car.travelTime}</td>
        <td>${avgSpeed.toFixed(2)}</td>
        <td class="ticket--${ticket}">${ticket}</td>
    `;
    resultsElem.appendChild(newHtmlElem);
}

function getAvgSpeed(km, time) {
    return km / time;
}

function addCar() {
    let formElem = document.getElementById('add_car');
    let carModel = formElem.elements.namedItem('model').value;
    let licPlate = formElem.elements.namedItem('plate').value;
    let traveled = Number(formElem.elements.namedItem('traveled').value);
    let traveltime = Number(formElem.elements.namedItem('time').value);

    let newCarIndex = carDB.push({
        model: carModel,
        traveled: traveled,
        travelTime: traveltime,
        licPlate: licPlate,
    });

    showInHTML(carDB[newCarIndex - 1]);
    formElem.elements.namedItem('model').value = '';
    formElem.elements.namedItem('plate').value = '';
    formElem.elements.namedItem('traveled').value = '';
    formElem.elements.namedItem('time').value = '';
}

function fillRandomValues() {
    let formElem = document.getElementById('add_car');
    let carModel = formElem.elements.namedItem('model').value;
    let licPlate = formElem.elements.namedItem('plate').value;
    let traveled = formElem.elements.namedItem('traveled').value;
    let traveltime = formElem.elements.namedItem('time').value;

    // Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

    formElem.elements.namedItem('model').value = carModels[getRandomInt(0, carModels.length - 1)];
    formElem.elements.namedItem('plate').value = Math.random().toString(36).substring(2, 5) + '-'
        + Math.random().toFixed(3).toString().substring(2);
    formElem.elements.namedItem('traveled').value = getRandomInt(1, 2000);
    formElem.elements.namedItem('time').value = getRandomInt(1, 48);
}

function getRandomInt(min, max) {
    /* return Math.floor(Math.random() * Math.floor(max)); */
    return Math.floor(Math.random() * (Math.floor(max) - Math.ceil(min) + 1)) + Math.ceil(min);
}