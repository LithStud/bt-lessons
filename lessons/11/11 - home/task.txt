Parašykite programą, kurioje masyve būtų saugomi mašinų objektai.
Kiekvienoje mašinoje saugomi nuvažiuoto kelio ir laiko (kiek mašina užtruko kelyje)
duomenys bei mašinos valstybiniai numeriai. 

Atspausdinkite duotus mašinų duomenis bei apskaičiuokite vidutinį kiekvienos mašinos greitį ir jį taip pat atspausdinkite.
Jei vidutinis mašinos greitis viršija 60, atspausdinkite mašinos valstybinius numerius.