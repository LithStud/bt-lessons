let options = {
    resize: true,
    speed: 300,
    theme: 'slinky-theme-default',
    title: false
}
const slinky = $('#menu').slinky({ title: true });

$(document).ready(function () {
    $('.tooltip').tooltipster({
        animation: 'fall',
        delay: 200,
        theme: 'tooltipster-punk',
        trigger: 'hover'
    });
});