document.addEventListener('DOMContentLoaded', (e) => {
    console.log('DOM Loaded');

    document.getElementById('calc')
        .addEventListener('click', (e) => {
            console.log(document.getElementById('width').value);
            calculate();
        });
});

function calculate() {
    let widthElem = document.getElementById('width');
    let heightElem = document.getElementById('height');
    let resultsElem = document.getElementById('results');
    let width = Number(widthElem.value);
    let height = Number(heightElem.value);

    cleanStyle(widthElem);
    cleanStyle(heightElem);

    if (!resultsElem.classList.contains('hidden'))
        resultsElem.classList.add('hidden');

    if (!width || !height) {
        if (!width) addBad(widthElem);
        if (!height) addBad(heightElem);
        alert('Nothing entered');
        return;
    }

    if (width <= 0 || height <= 0) {
        alert('Not posible dimensions');
        if (width <= 0) {
            addBad(widthElem);
        }
        if (height <= 0) {
            addBad(heightElem);
        }
        return;
    }

    let perimeter = width * 2 + height * 2;
    let area = width * height;
    let c = Math.sqrt(width * width + height * height);

    console.log(`Perimeter: ${perimeter}, Area: ${area}, C: ${c}`);

    showResults(perimeter, area, c);
}

function showResults(p, a, c) {
    let resultsElem = document.getElementById('results');
    let pElem = document.getElementById('perimeter');
    let aElem = document.getElementById('area');
    let cElem = document.getElementById('cresult');

    pElem.innerHTML = p;
    aElem.innerHTML = a;
    cElem.innerHTML = c;

    resultsElem.classList.remove('hidden');
}

function addBad(elem) {
    if (!elem.classList.contains('bad')) {
        elem.classList.add('bad');
    }
}

function cleanStyle(elem) {
    elem.classList.remove('bad');
}