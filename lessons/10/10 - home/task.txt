Parašykite programą, kuri turėtų du kintamuosius, saugančius stačiakampio pločio ir aukščio reikšmes.
Panaudodami matematines operacijas ir duotus kintamuosius apskaičiuokite stačiakampio perimetrą,
plotą ir įstrižainę, išsaugokite šiuos duomenis kintamuosiuose.
Pasirinktu būdu atspausdinkite visus stačiakampio duomenis.