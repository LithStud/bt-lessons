let UI = {
    word: document.getElementById('word'),
    bar: document.getElementById('loadingBar'),
    message: document.getElementById('message'),
}

let game = {
    word: 'bomba',
    progress: 0,
    drawLetters() {
        UI.word.innerHTML = '';
        for (let i = 0; i < this.word.length; i++) {
            UI.word.innerHTML += '<div class="letter"></div>';
        }
    },

    /**
     * 
     * @param {String} letter letter
     */
    checkLetter(letter) {
        let letterFound = false;
        for (let i = 0; i < this.word.length; i++) {
            if (this.word[i] === letter) {
                // Letter Correct
                console.log('Correct');
                letterFound = true;
                UI.word.children[i].innerHTML = letter;
            }
        }


        if (!letterFound)
            this.addProgress(10);

        // CHeck for win
        for (let letter of UI.word.children) {
            if (!letter.innerHTML) {
                return;
            }
        }

        UI.message.innerHTML = 'Not bad... Keep on going, smartass!';
        setTimeout(() => {
            this.init();
        }, 2000);
    },

    addProgress(amount) {
        this.progress += amount;
        this.progress = Math.min(this.progress, 100);
        this.drawProgress();

        if (this.progress === 100) {
            UI.message.innerHTML = 'Sucks to be you...';
            this.init();
        }
    },

    drawProgress() {
        UI.bar.style.width = this.progress + '%';
    },

    init() {
        let words = ['bomba', 'teroristas', 'alach', 'virgin', 'isis', 'goat'];
        let randomIndex = Math.floor(Math.random() * words.length);
        this.word = words[randomIndex];
        this.drawLetters();
        this.progress = 0;
        this.drawProgress();
    }


}

document.addEventListener('keyup', e => {
    game.checkLetter(e.key);
});

setInterval(() => {
    game.addProgress(1);
}, 200);

// Start game
game.init();