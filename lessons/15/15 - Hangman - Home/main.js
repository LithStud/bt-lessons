/**
 * UI specific pointers to HTML elements or audio sounds
 */
let UI = {
    lvlSelect: document.getElementById('lvlSelect'),
    lvlSelectTooltip: document.getElementById('selectionTooltip'),
    lvlButtons: document.querySelectorAll('.lvlBtn'), // points to HTML nodes array
    word: document.getElementById('word'),
    wordTip: document.getElementById('wordTip'),
    score: document.getElementById('score'),
    usedLetters: document.getElementById('usedLetters'),
    lossScreen: document.getElementById('lossScreen'),
    answer: document.getElementById('answer'),
    finalScore: document.getElementById('finalScore'),
    usedDifficulty: document.getElementById('usedDifficulty'),
    winCount: document.getElementById('winCount'),
    newGameBtn: document.getElementById('newGameBtn'),
    sound: document.getElementById('sound'),
    sounds: {
        click: new Audio('https://freesound.org/data/previews/260/260138_4486188-lq.mp3'),
        success: new Audio('https://freesound.org/data/previews/146/146721_2437358-lq.mp3'),
        win: new Audio('https://freesound.org/data/previews/406/406371_7873941-lq.mp3'),
        loss: new Audio('https://freesound.org/data/previews/178/178875_1400623-lq.mp3')
    },
    human: { // svg parts of body
        head: document.getElementById('head'),
        body: document.getElementById('body'),
        lHand: document.getElementById('lHand'),
        rHand: document.getElementById('rHand'),
        lLeg: document.getElementById('lLeg'),
        rLeg: document.getElementById('rLeg'),
    },

    /**
     * Switches SVG icon according to if it should show sound on or off
     * @param {Boolean} isOn is sound on or off
     */
    changeSoundIcon(isOn) {
        this.sound.children[0].src = `svg/sound-${isOn ? 'on' : 'off'}.svg`;
    }
}

/**
 * Various possible game states
 */
let states = {
    lvlSelect: 'lvlSelect',
    game: 'game',
    loss: 'loss',
    win: 'win'
}

/**
 * Difficulty levels (used for loss screen representation of chosen difficulty)
 */
let diffLevels = ['Easy', 'Medium', 'Hard'];

/**
 * Words List (once its fetched from json file)
 */
let words = [];

/**
 * Game Object
 */
let game = {
    state: states.lvlSelect, // Game state - 'lvlSelect', 'game', 'loss', 'win'
    word: 'bomba', // Word to be guessed
    usedLetters: '', // Letters that were used nad wrong
    sound: true, // Sound On/Off
    mistakes: 0, // Mistakes made (max 6)
    wins: 0, // Words completed
    dificulty: 0, // Difficulty level 0 - Easy, 1 - Medium, 2 - Hard
    score: 0, // Total score (Score + Word Lenght)
    // Body parts, associated with mistakes
    bodyParts: ['head', 'body', 'lHand', 'rHand', 'lLeg', 'rLeg'],

    /**
     * Draw chosen word as empty spaces
     */
    drawLetters() {
        UI.word.innerHTML = '';
        UI.usedLetters.innerHTML = ''; // Clean up guessed letters
        for (let i = 0; i < this.word.length; i++) {
            UI.word.innerHTML += '<div class="letter"></div>';
        }
    },

    /**
     * Checks if letter is in word
     * @param {String} letter letter
     */
    checkLetter(letter) {
        letter = letter.toLowerCase(); // always use lower case
        let letterFound = false;
        for (let i = 0; i < this.word.length; i++) {
            if (this.word[i] === letter) {
                // Letter Correct
                letterFound = true;
                UI.word.children[i].innerHTML = letter;
                this.playSound(UI.sounds.success);
            }
        }

        // If letter wrong add to wrong letters list and draw another body part as
        // punishement
        if (!letterFound) {
            // only penalize on new mistaken letters
            if (!this.usedLetters.includes(letter)) {
                UI.human[this.bodyParts[this.mistakes]].classList.add('animate');
                this.usedLetters += letter;
                UI.usedLetters.innerHTML += `<div class="letter">${letter}</div>`;
                this.addMistake(1);
            }
            this.playSound(UI.sounds.click);
        }

        // Check for win
        for (let letter of UI.word.children) {
            if (!letter.innerHTML) {
                // not all letters guessed
                return;
            }
        }

        // Game Won
        this.playSound(UI.sounds.win);
        // increase score by word length
        this.score += this.word.length;
        // increase win count
        this.wins++;
        this.state = states.win;
        // start new game after short pause
        setTimeout(() => {
            this.init();
        }, 2000);
    },

    /**
     * Increase mistake count (1 mistake = 1 body part)
     * @param {Number} amount How many mistakes to add
     */
    addMistake(amount) {
        this.mistakes += amount;

        // if all body parts used lose game
        if (this.mistakes > 5) {
            this.playSound(UI.sounds.loss);
            this.state = states.loss;
            // show loss screen after short pause
            setTimeout(() => {
                this.init();
            }, 2000);
        }
    },

    /**
     * Play given sound
     * @param {Audio} sound HTML audio element
     */
    playSound(sound) {
        // check if sound is on
        if (game.sound)
            sound.play();
    },

    /**
     * Resets Human parts in svg
     */
    resetHuman() {
        for (part in UI.human) {
            UI.human[part].classList.remove('animate');
        }
    },

    /**
     * Randomly assigns word from word list according to selected difficulty
     */
    selectWord() {
        let found = false;
        let randomIndex = null;
        let i = 0;
        let maxTries = 10000; // try this many times before giving up
        while (!found && i < maxTries) {
            randomIndex = Math.floor(Math.random() * words.length);
            switch (game.dificulty) {
                // 1 - Medium ( 3 - 6 )
                case 1:
                    if (words[randomIndex].name.length >= 3 && words[randomIndex].name.length <= 6)
                        found = true;
                    break;
                // 2 - Hard
                case 2:
                    if (words[randomIndex].name.length >= 6)
                        found = true;
                    break;
                // 0 - Easy
                default:
                    if (words[randomIndex].name.length <= 3)
                        found = true;
                    break;
            }
            i++;
        }

        // In case didnt found word matching requirements
        if (!found) {
            console.error(`Error: Gave up on finding word for selected difficulty (${this.dificulty}), using last random word`);
        }

        this.word = words[randomIndex].name.toLowerCase();
        UI.wordTip.innerHTML = words[randomIndex].detail;
        //console.log(this.word);
        //console.log(words[randomIndex].detail);
    },

    /**
     * Update score
     */
    updateScore() {
        UI.score.innerHTML = this.score;
    },

    /**
     * Initiate game elements according to set state
     */
    init() {
        switch (this.state) {
            // Gameplay state
            case states.game:
                this.selectWord();

                this.score = 0;
                this.mistakes = 0;
                this.usedLetters = '';
                UI.score.innerHTML = this.score;
                this.resetHuman();
                this.drawLetters();

                document.addEventListener('keyup', this.keyboardHook);

                break;

            // Lost game state
            case states.loss:
                document.removeEventListener('keyup', this.keyboardHook);
                UI.lossScreen.classList.remove('hidden');
                UI.finalScore.innerHTML = game.score;
                UI.winCount.innerHTML = game.wins;
                UI.usedDifficulty.innerHTML = diffLevels[game.dificulty];
                UI.answer.innerHTML = game.word;

                break;

            // Win state
            case states.win:
                this.updateScore();
                this.selectWord();

                this.usedLetters = '';
                // Only reset mistakes if difficulty isnt Hard
                if (this.dificulty != 2) {
                    this.mistakes = 0;
                    this.resetHuman();
                }
                this.drawLetters();
                this.state = states.game;
                break;

            // Difficulty selection state
            default:
                document.removeEventListener('keyup', this.keyboardHook);
                UI.lvlSelect.classList.remove('hidden');
                break;
        }
    },

    /**
     * Method used in keyboard hood (as separate method so it can be unregistered)
     * @param {KeyboardEvent} e Keyboard event
     */
    keyboardHook(e) {
        let alphaOnly = /^[A-Za-z]{1}$/g;
        if (e.key.match(alphaOnly)) {
            game.checkLetter(e.key);
        }
    },


}

document.addEventListener('DOMContentLoaded', e => {
    // Default sound state setup
    UI.changeSoundIcon(game.sound);

    // Fetch word list
    fetch('./data/vocabulary-words.json')
        .then(response => {
            return response.json();
        })
        .then(json => {
            words = json.data;
            // Start game
            game.state = states.lvlSelect;
            game.init();
        })
        .catch(err => {
            console.error(`Error occured:`);
            console.error(err);
        });

    // Difficulty buttons hook
    UI.lvlButtons.forEach(button => {

        button.addEventListener('click', e => {
            game.dificulty = Number(e.target.dataset.lvl);
            game.state = states.game;
            game.init();
            UI.lvlSelect.classList.add('hidden');
        });

        button.addEventListener('mouseenter', e => {
            UI.lvlSelectTooltip.innerHTML = e.target.dataset.tip;
        });

        button.addEventListener('mouseleave', e => {
            UI.lvlSelectTooltip.innerHTML = '';
        });
    });

    // Sound button
    UI.sound.addEventListener('click', e => {
        game.sound = game.sound ? false : true;
        UI.changeSoundIcon(game.sound);
    });

    // Restart game button
    UI.newGameBtn.addEventListener('click', e => {
        UI.lossScreen.classList.add('hidden');
        game.state = states.lvlSelect;
        game.init();
    });
});