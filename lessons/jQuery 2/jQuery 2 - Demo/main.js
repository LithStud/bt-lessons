// 1
/* let jsonWork = {
    raktas: 0,
    vardas: "Jonas",
    klase: "A",
    turtas: {
        kilnojamas: 500,
        nekilnojamas: 300
    },
    vaikai: ['Ona', 'Petriukas']
}

console.log(jsonWork);
// 1.1
let jsonString = JSON.stringify(jsonWork);
console.log(jsonString);
// 1.2
console.log(JSON.parse(jsonString)); */

let data = null;

function success(json) {
    data = json;
    $('#dataBtn').removeAttr('disabled').text('Show Data');
}

$('#dataBtn').on('click', function (e) {
    $('#classwork').append(`<img src="${data.url}" alt="${data.title}">`);
    $('#classwork').append(`<div>${JSON.stringify(data)}</div>`);
    $(this).attr('disabled', '').text('DATA BEING SHOWN');
});

$.getJSON('https://api.nasa.gov/planetary/apod?api_key=NNKOjkoul8n1CH18TWA9gwngW1s1SmjESPjNoUFo')
    .done(success)
    .fail(function (error) {
        console.log('Request failed with error:');
        console.log(error.status, error.statusText);
        $('#dataBtn').text('Failed to load');
    });