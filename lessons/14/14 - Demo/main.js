let chatBox = document.getElementById('chatBox');
let button = document.getElementById('button');
let input = document.getElementById('input');

chatBox.innerHTML = 'Welcome to Chat!';

function sendMessage() {
    let reg = /\:\)/g;
    let msg = input.value.replace(reg, '😃');
    if (!msg) return;
    let date = new Date();
    chatBox.innerHTML += `<div>${date.getHours()}:${date.getMinutes()} &gt; ${msg}</div>`;
    input.value = '';
}

button.addEventListener('click', sendMessage);

input.addEventListener('keypress', e => {
    if (e.key === 'Enter') {
        sendMessage();
    }
});