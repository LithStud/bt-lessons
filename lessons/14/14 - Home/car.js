class Car {
    /**
     * Car object
     * @param {String} model Car model
     * @param {Number} speed Car speed, KM/H
     * @param {Number} totalKm Optional. How far car has traveled so far in KM
     */
    constructor(model, speed, totalKm = 0) {
        this.model = model;
        this.speed = speed;
        this.totalKm = totalKm;
    }

    /**
     * Calculates how far car will drive in a specified time.
     * @param {Number} time Total time car drives (H)
     */
    drive(time) {
        this.totalKm += time * this.speed;
    }

    /**
     * Prints car information to console
     */
    printData() {
        console.log('======[ Car Info ]======');
        console.log(`Car Model: ${this.model}`);
        console.log(`Speed: ${this.speed}`);
        console.log(`Total KM: ${this.totalKm}`);
        console.log('============================');
    }
}