const minSpeed = 150;
const maxSpeed = 260;
const minTime = 1;
const maxTime = 100;

const carModels = ['Audi', 'BMW', 'Lamborghini', 'Moskwich', 'Volkswagen', 'Ford', 'Tesla'];

/**
 * Car Database
 * @type {Car[]}
 */
let carDb = [];

// Check that dom is trully loaded
document.addEventListener('DOMContentLoaded', (e) => {
    console.log('DOM Loaded');

    // create initial table
    updateHtml();

    // Hook into menu button
    document.getElementById('car_form_btn').addEventListener('click', (e) => {
        document.getElementById('car_form').classList.toggle('open');
    });

    // Hook random values button
    document.getElementById('random_btn').addEventListener('click', (e) => {
        e.preventDefault();
        fillRandomValues();
    });

    // Hook race button
    document.getElementById('race_btn').addEventListener('click', (e) => {
        e.preventDefault();
        raceEvent();
        updateHtml();
    });

    // Hook Car adding button
    document.getElementById('add_car_btn').addEventListener('click', (e) => {
        e.preventDefault();
        addCar();
        updateHtml();
    });

    // Hook into future trash buttons
    document.addEventListener('click', e => {
        if (e.target.classList.contains('trash')) {
            carDb.splice(e.target.dataset.index, 1);
            updateHtml();
        }
    });
});

/**
 * Adds new Car into car database
 */
function addCar() {
    let formElem = document.getElementById('car_form');
    let carModel = formElem.elements.namedItem('model').value;
    let carSpeed = Number(formElem.elements.namedItem('speed').value);

    carDb.push(new Car(carModel, carSpeed));

    carDb[carDb.length - 1].printData();

    // sort array
    carDb.sort((a, b) => { return a.totalKm - b.totalKm; });
}

/**
 * Drives all cars in database
 */
function raceEvent() {
    let formElem = document.getElementById('car_form');
    let raceTime = Number(formElem.elements.namedItem('race_time').value);

    carDb.forEach(car => {
        car.drive(raceTime);
    });
}

/**
 * Updates html with car databases values
 */
function updateHtml() {
    let resultsElem = document.getElementById('results');

    // clean previous results
    resultsElem.innerHTML = '';

    // notification if car database is empty
    if (!carDb.length) {
        resultsElem.innerHTML = `
            <tr><td colspan="4">No Cars in Database</td></tr>
        `;
        return;
    }

    carDb.forEach((car, index) => {
        let newHtmlElem = document.createElement('tr');
        newHtmlElem.innerHTML = `
            <td>${car.model}</td>
            <td>${car.speed}</td>
            <td>${car.totalKm}</td>
            <td><img class="trash" data-index="${index}" src="img/trash.svg"></td>
        `;
        resultsElem.appendChild(newHtmlElem);
    });
}

/**
 * Randomly fills in values for inputs
 */
function fillRandomValues() {
    let formElem = document.getElementById('car_form');
    let carModel = formElem.elements.namedItem('model');
    let carSpeed = formElem.elements.namedItem('speed');
    let raceTime = formElem.elements.namedItem('race_time');

    carModel.value = carModels[getRandomInt(0, carModels.length - 1)];
    carSpeed.value = getRandomInt(minSpeed, maxSpeed);
    raceTime.value = getRandomInt(minTime, maxTime);
}

/**
 * Generates random number between min and max (inclusive)
 * @param {Number} min Minimum number
 * @param {Number} max Maximum number
 */
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}