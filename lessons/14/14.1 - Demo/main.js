'use strict';

class Product {
    constructor(name, price, quantity, category) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.category = category;
    }

    printData() {
        console.log('====[ PRODUCT ]====');
        console.log(`Name: ${this.name}`);
        console.log(`Price: ${this.price}`);
        console.log(`Quantity: ${this.quantity}`);
        console.log(`Category: ${this.category}`);
        console.log('===================');
    }

    addQuantity(amout) {
        this.quantity += amount;
    }

    totalValue() {
        return this.quantity * this.price;
    }
}

/**
 * @type {Product[]}
 */
let products = [];

let button = document.getElementById('addProduct');
let nameInput = document.getElementById('nameInput');
let priceInput = document.getElementById('priceInput');
let quatityInput = document.getElementById('quantityInput');
let categoryInput = document.getElementById('categoryInput');
let table = document.getElementById('table');

button.addEventListener('click', e => {
    console.log('Adding product');
    let product = new Product(nameInput.value, priceInput.value * 1, quatityInput.value * 1, categoryInput.value);

    products.push(product);

    printProducts();
    drawProducts();
});

function printProducts() {
    console.clear();
    for (let product of products) {
        product.printData();
    }
}

function drawProducts() {
    table.innerHTML = '';

    for(let product of products) {
        table.innerHTML += `<tr>
            <td>${product.name}</td>
            <td>${product.price}</td>
            <td>${product.quantity}</td>
            <td>${product.category}</td>
            <td>${product.totalValue()}</td>
        </tr>`;
    }
}